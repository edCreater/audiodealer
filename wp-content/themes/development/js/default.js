
(function($){

    "use strict";

    /**
     *  Document Ready
     */
    $(document).ready(function(){
        /**
         * Mobile Navigation
         */
        $('body').on('click', '.js-show-mobile-navigation', function(e){
            if( $(this).hasClass('is-active') ){
                $(this).removeClass('is-active');
                $('.navigation').removeClass('opened');
            }
            else{
                $(this).addClass('is-active');
                $('.navigation').addClass('opened');
            }
        });

        /**
         * Slider Init
         */

        var homeSlider = new Swiper('.h_slider', {
            pagination: '.h_slider__pagination',
            paginationClickable: true
        });

        var productsSlider = new Swiper('.products__slider', {
            slidesPerView: 4,
            slidesPerColumn: 2,
            spaceBetween: 30,
            nextButton: '.products__section .slider__control.next',
            prevButton: '.products__section .slider__control.prev',
            loop: true,
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            // when window width is <= 640px
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 30
            }
        });

        var projectsSlider = new Swiper('.projects__slider', {
            slidesPerView: 4,
            spaceBetween: 30,
            nextButton: '.projects__section .slider__control.next',
            prevButton: '.projects__section .slider__control.prev',
            loop: true,
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            // when window width is <= 640px
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 30
            }
        });

    });

})(jQuery);

