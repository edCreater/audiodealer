<?php
/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 24.09.2016
 */

namespace SweetCore\Libs;


class CS_Libs
{
  function __construct() {

    if(!class_exists('ReduxFramework'))
      require_once ( dirname( __FILE__ ) . '/ReduxCore/framework.php' );

    require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

    require_once dirname( __FILE__ ) . '/meta-box-class/my-meta-box-class.php';

    require_once dirname( __FILE__ ) . '/tax-meta-class/tax-meta-class.php';

  }
}