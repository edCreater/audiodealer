<?php
function mtranse_categories_array(){

  $terms = get_terms( 'category', array(
      'hide_empty' => false,
  ) );

  $categories = array();

  foreach ($terms as $term ){
    $categories[$term->term_id] = $term->name;
  }

  return $categories;

}

/**
 * Home Options
 * 
 * @author Chinh Duong Manh
 */
/* Start Dummy Data*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
$msg = $disabled = '';
if (!class_exists('WPBakeryVisualComposerAbstract') or !class_exists('CmssuperheroesCore') or !function_exists('cptui_create_custom_post_types')){
    $disabled = ' disabled ';
    $msg='<p class="required">You should be install & active <strong>CMSSuperHeroes, Visual Composer and Custom Post Type UI</strong> plugins to import our demo data.</p> <p>If you want to install all our demo data, please install all recommended plugin too !</p>';
}

$this->sections[] = array(
    'icon' => 'el-icon-briefcase',
    'title' => esc_html__('Demo Content', 'mtranse'),
    'fields' => array(
        array(
            'subtitle' => '<input type=\'button\' name=\'sample\' id=\'dummy-data\' '.$disabled.' value=\'Import Now\' /><div class=\'cms-dummy-process\'><div  class=\'cms-dummy-process-bar\'></div></div><div id=\'cms-msg\'><span class="cms-status"></span>'.$msg.'</div>',
            'id' => 'theme',
            'icon' => true,
            'default' => 'wp_puro',
            'options' => array(
                'wp_puro' => get_template_directory_uri().'/assets/images/dummy/01.jpg',
                'wp_puro2' => get_template_directory_uri().'/assets/images/dummy/02.jpg',
                'wp_puro3' => get_template_directory_uri().'/assets/images/dummy/03.jpg',
            ),
            'type' => 'image_select',
            'title' => 'Select Theme'
        )
    )
);
/* End Dummy Data*/


/**
 * Common content
 */
$this->sections[] = array(
    'title' => esc_html__('Common content', 'mtranse'),
    'icon' => 'el-icon-star',
    'fields' => array(
        array(
            'title' => esc_html__('Icon', 'mtranse'),
            'subtitle' => esc_html__('Select a favicon icon (.png, .jpg).', 'mtranse'),
            'id' => 'mtranse_favicon_icon',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url'=>get_template_directory_uri().'/assets/images/favicon.ico'
            )
        ),
        array(
            'id'       => 'mtranse_news_cat',
            'type'     => 'select',
            'title'    => __('News', 'mtranse'),
            'subtitle' => __('Category of news', 'mtranse'),
            'desc'     => __('Category of news', 'mtranse'),
            'options'  => mtranse_categories_array(),
            'default'  => '',
        ),
        array(
            'title' => esc_html__('News header image', 'mtranse'),
            'subtitle' => esc_html__('Select an image file for news header.', 'mtranse'),
            'id' => 'mtranse_news_header_img',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url'=>THEME_DIR_URI.'/img/news-bg.jpg'
            ),
        ),
        array(
            'id'       => 'mtranse_reviews_cat',
            'type'     => 'select',
            'title'    => __('Reviews', 'mtranse'),
            'subtitle' => __('Category of reviews', 'mtranse'),
            'desc'     => __('Category of reviews', 'mtranse'),
            'options'  => mtranse_categories_array(),
            'default'  => '',
        ),
        array(
            'title' => esc_html__('Articles header image', 'mtranse'),
            'subtitle' => esc_html__('Select an image file for articles header.', 'mtranse'),
            'id' => 'mtranse_articles_header_img',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url'=>THEME_DIR_URI.'/img/articles-bg.jpg'
            ),
        ),

    )
);
/**


/**
 * Header Options
 * 
 * @author Chinh Duong Manh
 */
$this->sections[] = array(
    'title' => esc_html__('Header', 'mtranse'),
    'icon' => 'el-icon-credit-card',
    'fields' => array(
        array(
            'title' => esc_html__('Select Logo', 'mtranse'),
            'subtitle' => esc_html__('Select an image file for your logo.', 'mtranse'),
            'id' => 'mtranse_main_logo',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url'=>THEME_DIR_URI.'/img/logo.png'
            ),
        ),
        array(
            'id'       => 'mtranse_slogan',
            'type'     => 'textarea',
            'title'    => __('Slogan', 'mtranse'),
            'subtitle' => __('Slogan in header', 'mtranse'),
            'desc'     => __('Slogan in header.', 'mtranse'),
            'validate' => 'html_custom',
            'default'  => 'Вывоз мусора Москва и Московская область.<br> Оперативно и качественно!'
        ),
        array(
            'id'       => 'mtranse_phone',
            'type'     => 'textarea',
            'title'    => __('Phone', 'mtranse'),
            'subtitle' => __('Phone in header', 'mtranse'),
            'desc'     => __('Phone in header.', 'mtranse'),
            'validate' => 'html_custom',
            'default'  => '<span>+7 (495)</span><span> 724-09-91</span>'
        ),
        array(
            'id'       => 'mtranse_work_time',
            'type'     => 'textarea',
            'title'    => __('Work Time', 'mtranse'),
            'subtitle' => __('Work time in header', 'mtranse'),
            'desc'     => __('Work time in header.', 'mtranse'),
            'validate' => 'html_custom',
            'default'  => 'Прием заявок ежедневно с <span>9.00 до 22.00</span>'
        ),
        array(
            'id'       => 'mtranse_weekend',
            'type'     => 'textarea',
            'title'    => __('Weekend', 'mtranse'),
            'subtitle' => __('Weekend in header', 'mtranse'),
            'desc'     => __('Weekend in header.', 'mtranse'),
            'validate' => 'html_custom',
            'default'  => 'Вывоз мусора круглосуточно, без выходных'
        ),

    )
);

/**
 * Home Page
 * 
 * Archive, Pages, Single, 404, Search, Category, Tags .... 
 * @author Chinh Duong Manh
 */
$this->sections[] = array(
    'title' => esc_html__('Homepage', 'mtranse'),
    'icon' => 'el-icon-compass',
    'subsection' => false,
    'fields' => array(
        array(
            'id'       => 'mtranse_services',
            'type'     => 'textarea',
            'title'    => __('Services', 'mtranse'),
            'subtitle' => __('List of Servces', 'mtranse'),
            'desc'     => __('List of Servces', 'mtranse'),
            'validate' => 'html_custom',
            'msg'      => 'custom error message',
            'default'  => ''
        ),
        array(
            'id'       => 'mtranse_fb_link',
            'type'     => 'text',
            'title'    => __('Facebook link', 'mtranse'),
            'subtitle' => __('Link to facebook', 'mtranse'),
            'desc'     => __('Link to facebook  (socials group)', 'mtranse'),
            'validate' => 'url',
            'msg'      => 'custom error message',
            'default'  => 'http://google.com'
        ),
        array(
            'id'       => 'mtranse_inst_link',
            'type'     => 'text',
            'title'    => __('Instagram link', 'mtranse'),
            'subtitle' => __('Link to instagram', 'mtranse'),
            'desc'     => __('Link to instagram  (socials group)', 'mtranse'),
            'validate' => 'url',
            'msg'      => 'custom error message',
            'default'  => 'http://google.com'
        ),
    )
);


/**
 * Footer
 *
 * @author Chinh Duong Manh
 */
$this->sections[] = array(
    'title' => esc_html__('Footer', 'mtranse'),
    'icon' => 'el-icon-credit-card',
    'fields' => array(
        array(
            'title' => esc_html__('Select Logo', 'mtranse'),
            'subtitle' => esc_html__('Select an image file for your logo in footer.', 'mtranse'),
            'id' => 'mtranse_footer_logo',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url'=>get_template_directory_uri().'/img/logo.png'
            ),
        ),
        array(
            'id'       => 'mtranse_copiryght',
            'type'     => 'editor',
            'title'    => __('Text copyright', 'mtranse'),
            'subtitle' => __('Copyright in footer', 'mtranse'),
            'desc'     => __('Copyright in footer', 'mtranse'),
            'validate' => 'html',
            'msg'      => 'custom error message',
            'default'  => 'Text copyright',
            'args'   => array(
                'teeny'            => true,
                'textarea_rows'    => 5
            )
        ),
        array(
            'id'       => 'mtranse_address',
            'type'     => 'text',
            'title'    => __('Address', 'mtranse'),
            'subtitle' => __('Address string', 'mtranse'),
            'desc'     => __('Address string', 'mtranse'),
            'default'  => ''
        ),
        array(
            'id'       => 'mtranse_email',
            'type'     => 'text',
            'title'    => __('Email', 'mtranse'),
            'subtitle' => __('Email string', 'mtranse'),
            'desc'     => __('Email string', 'mtranse'),
            'default'  => ''
        ),


    )
);

