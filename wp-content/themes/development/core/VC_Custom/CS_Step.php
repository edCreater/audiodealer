<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Step
{

    public $params;

    function __construct(){
        $this->initParams();
        add_action('admin_init', array($this, 'step_init'));
        add_shortcode('cs_step', array($this, 'cs_step_shortcode'));
    }

    function initParams(){

        $this->params = array(
            'name' => 'Step',
            'description' => 'Step',
            'base' => 'cs_step',
            'icon' => 'cs-step',
            'category' => __( 'Content', 'cs'),
            'params' => array(
                array(
                    'type' => 'attach_image',
                    'heading' => __('Ico image', 'cs'),
                    'param_name' => 'icon_img'
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Title', 'cs'),
                    'value' => 'This is title',
                    'param_name' => 'title'
                ),
                array(
                    'type' => "textarea",
                    'heading' => __('Content', 'cs'),
                    'param_name' => 'icon_content',
                    'value' => ''
                ),


            )
        );
    }

    function step_init(){
        if (function_exists('vc_map')){
            vc_map($this->params);
        }
    }

    function cs_step_shortcode($atts, $content = null){
        $output = $icon_img = $img_tag  = $title = $icon_content = '';
        extract(shortcode_atts(array(
            'icon_img' => '',
            'title' => '',
            'icon_content' => '',
        ), $atts));

        if ($icon_img) {
            $icon_img = wp_get_attachment_image_src($icon_img, 'large');
            $icon_img = $icon_img[0];
            $img_tag .= '<img src="' . $icon_img . '" alt="' . $title . '">';
        }

        ob_start();
        ?>
        <div class="cs-step">
            <div class="cs-step-heading">
                <div class="cs-step-thumb">
                    <?php echo $img_tag; ?>
                </div>
                <div class="cs-step-title">
                    <h3><?php echo $title; ?></h3>
                </div>

            </div>
            <div class="cs-step-content">
                <?php echo $icon_content; ?>
            </div>
        </div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}