<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Posts_Grid
{

    public $params;

    function __construct(){
        $this->initParams();
        add_action('admin_init', array($this, 'posts_grid_init'));
        add_shortcode('posts_grid_box', array($this, 'cs_posts_grid_shortcode'));
    }

    function initParams(){

        $this->params = array(
            'name' => 'CS Posts Grid',
            'description' => 'CS Posts Grid',
            'base' => 'posts_grid_box',
            'icon' => 'cs-posts-grid',
            'category' => __( 'Content', 'cs'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Posttype', 'cs'),
                    'value' => 'This is posttype',
                    'param_name' => 'posttype'
                ),

            )
        );
    }

    function posts_grid_init(){
        if (function_exists('vc_map')){
            vc_map($this->params);
        }
    }

    function cs_posts_grid_shortcode($atts, $content = null){
        $output = $posttype = '';
        extract(shortcode_atts(array(
            'posttype' => ''
        ), $atts));

        if ($posttype) {
            $args = array(
                'post_type' => $posttype,
                'posts_per_page' => -1
            );
            $cs_posts = new \WP_Query($args);
            $cs_posts_count = $cs_posts->post_count;
        }

        ob_start();
        if ($cs_posts->have_posts()) : ?>
            <?php $i = 0; ?>
            <div class="cs-posts-grid">
                <?php while ($cs_posts->have_posts()) : $cs_posts->the_post(); ?>
                    <?php if( ($i % 3) == 0) : ?>
                    <div class="row content-row">
                        <div class="codesweet">
                            <div style="float: left">

                            </div>
                        </div>
                    <?php endif ?>
                    <div class="col-sm-8">
                        <div class="port-item">
                            <?php if (get_the_post_thumbnail(get_the_ID())) : ?>
                                <div class="thumb">
                                    <div class="thumb-wrap">
                                       <?php echo the_post_thumbnail('top-thumb', array('class'=>'img-responsive')); ?>
                                    </div>
                                    <div class="plus">
                                        <a href="<?php the_permalink(); ?>">
                                            <i class="fa fa-gg"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="title">
                                <h3><?php echo the_title(); ?></h3>
                                <p><?php the_field('subtitle'); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                    <?php if (( (($i) % 3) == 0) || ($cs_posts_count == $i)) : ?>
                        </div>
                    <?php endif ?>
                <?php endwhile; ?>
            </div>
            <?php
        endif;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}