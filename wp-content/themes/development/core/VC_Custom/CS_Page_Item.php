<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Page_Item
{

    public $params;

    function __construct(){
        $this->initParams();
        add_action('admin_init', array($this, 'page_item_init'));
        add_shortcode('cs_page_item', array($this, 'cs_page_item_shortcode'));
    }

    function initParams(){

        $this->params = array(
            'name' => 'Page Item',
            'description' => 'Page item',
            'base' => 'cs_page_item',
            'icon' => 'cs-page-item',
            'category' => __( 'Content', 'cs'),
            'params' => array(
                array (
                    'param_name' 	=> 'pageitem',
                    'type' 			=> 'postlist',
                    'heading' 		=> __('Select Page', 'mfn-opts'),
                    'admin_label'	=> true,
                    'query'			=> array(
                        'post_type'	=> 'page'
                    )
                ),

            )
        );
    }

    function page_item_init(){
        if (function_exists('vc_map')){
            vc_map($this->params);
        }
    }

    function cs_page_item_shortcode($atts, $content = null){
        $output = $pageitem = '';
        extract(shortcode_atts(array(
            'pageitem' => '',
        ), $atts));

        if ($pageitem) {
            $post_page = get_post( $pageitem );
        }

        ob_start();
        ?>
        <div class="port-item">
            <?php if (get_post_thumbnail_id($post_page->ID)) : ?>
                <div class="thumb">
                    <a href="<?php echo get_the_permalink($post_page->ID); ?>">
                        <span class="thumb-wrap">
                            <?php echo get_the_post_thumbnail($post_page->ID, 'top-thumb', array('class'=>'img-responsive')); ?>
                        </span>
                    </a>
                </div>
            <?php endif; ?>
            <?php $subtitle = get_field('subtitle', $post_page->ID, true); ?>
            <div class="title">
                <h3><?php echo get_the_title($post_page->ID); ?></h3>
                <p><?php echo $subtitle; ?></p>
            </div>
        </div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}