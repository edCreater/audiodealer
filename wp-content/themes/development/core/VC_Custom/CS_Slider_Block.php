<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Slider_Block
{

    public $params;

    function __construct()
    {
        $this->initParams();
        add_action('admin_init', array($this, 'slider_block_init'));
        add_shortcode('cs_slider_block', array($this, 'cs_slider_block_shortcode'));
    }

    function initParams()
    {

        $this->params = array(
            'name' => 'Slider block',
            'description' => 'Slider block',
            'base' => 'cs_slider_block',
            'icon' => 'cs-slider_block',
            'category' => __('Content', 'cs'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Title', 'cs'),
                    'value' => 'This is title',
                    'param_name' => 'title'
                ),
            )
        );
    }

    function slider_block_init()
    {
        if (function_exists('vc_map')) {
            vc_map($this->params);
        }
    }

    function cs_slider_block_shortcode($atts, $content = null)
    {
        $output = $title = '';
        extract(shortcode_atts(array(
            'title' => '',
        ), $atts));

        ob_start();
        $args = array(
            'post_type' => 'cs_slider',
            'posts_per_page' => -1
        );
        $slider = new WP_Query($args);
        if ($slider->have_posts()): ?>
            <div class="cs-slider-block">
                <div class="sl-overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-24">
                            <div class="swiper-container">
                                <div class="swiper-button-next" title="Вперед"></div>
                                <div class="swiper-button-prev" title="Назад"></div>
                                <div class="swiper-wrapper">

                                    <?php while ($slider->have_posts()) : $slider->the_post(); ?>
                                        <?php $sl_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
                                        <div class="swiper-slide sl-slide" data-back="<?php echo $sl_image[0]; ?>">
                                            <div class="sl-content">
                                                <div class="sl-content-inner">
                                                    <?php the_content(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}