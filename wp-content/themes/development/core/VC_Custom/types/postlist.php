<?php
add_shortcode_param( 'postlist', 'rst_posslist_settings_field' );
function rst_posslist_settings_field( $settings, $value ) {
	$html = '';
	
	$args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	);
	$args = array_merge($args, $settings['query']);
	
	$posts = get_posts($args);
	$html .= '<select class="wpb_vc_param_value" name="'. esc_attr( $settings['param_name'] ) .'">';
		$html .= '<option value="">Select Post</option>';
		if( $posts ) {
			foreach( $posts as $item ) {
				$html .= '<option '. selected($item->ID,$value) .' value="'. $item->ID .'">'. $item->post_title .'</option>';
			}
		}
	$html .= '</select>';
	
	
	return $html;
}