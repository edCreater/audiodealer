<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Portfolio_Item
{

    public $params;

    function __construct(){
        $this->initParams();
        add_action('admin_init', array($this, 'portfolio_item_init'));
        add_shortcode('cs_portfolio_item', array($this, 'cs_portfolio_item_shortcode'));
    }

    function initParams(){

        $this->params = array(
            'name' => 'Portfolio Item',
            'description' => 'Portfolio item',
            'base' => 'cs_portfolio_item',
            'icon' => 'cs-portfolio-item',
            'category' => __( 'Content', 'cs'),
            'params' => array(
                array (
                    'param_name' 	=> 'portitem',
                    'type' 			=> 'postlist',
                    'heading' 		=> __('Select Testimonial', 'mfn-opts'),
                    'admin_label'	=> true,
                    'query'			=> array(
                        'post_type'	=> 'cs_portfolio'
                    )
                ),

            )
        );
    }

    function portfolio_item_init(){
        if (function_exists('vc_map')){
            vc_map($this->params);
        }
    }

    function cs_portfolio_item_shortcode($atts, $content = null){
        $output = $portitem = '';
        extract(shortcode_atts(array(
            'portitem' => '',
        ), $atts));

        if ($portitem) {
            $post_portfolio = get_post( $portitem );
        }

        ob_start();
        ?>
        <div class="port-item">
            <?php if (get_post_thumbnail_id($post_portfolio->ID)) : ?>
                <div class="thumb">
                    <div class="thumb-wrap">
                        <?php echo get_the_post_thumbnail($post_portfolio->ID, 'top-thumb', array('class'=>'img-responsive')); ?>
                    </div>
                    <div class="plus">
                        <a href="<?php echo get_permalink($post_portfolio->ID); ?>">
                            <i class="fa fa-gg"></i>
                        </a>
                    </div>
                </div>
            <?php endif; ?>
            <?php $subtitle = get_field('subtitle', $post_portfolio->ID, true); ?>
            <div class="title">
                <h3><?php echo get_the_title($post_portfolio->ID); ?></h3>
                <p><?php echo $subtitle; ?></p>
            </div>
        </div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}