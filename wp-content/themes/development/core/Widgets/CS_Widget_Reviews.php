<?php
/**
 * Widget Reviews widget class
 *
 * @since 2.8.0
 */
class CS_Widget_Reviews extends WP_Widget {

  public function __construct() {
    $widget_ops = array('classname' => 'widget_cs_reviews_entries', 'description' => __( "Reviews list.") );
    parent::__construct('reviews-widget', __('Reviews Widget'), $widget_ops);
    $this->alt_option_name = 'widget_cs_reviews_entries';

    add_action( 'save_post', array($this, 'flush_widget_cache') );
    add_action( 'deleted_post', array($this, 'flush_widget_cache') );
    add_action( 'switch_theme', array($this, 'flush_widget_cache') );
  }

  public function widget( $args, $instance ) {

    $cache = array();
    if ( ! $this->is_preview() ) {
      $cache = wp_cache_get( 'widget_reviews', 'widget' );
    }

    if ( ! is_array( $cache ) ) {
      $cache = array();
    }

    if ( ! isset( $args['widget_id'] ) ) {
      $args['widget_id'] = $this->id;
    }

    if ( isset( $cache[ $args['widget_id'] ] ) ) {
      echo $cache[ $args['widget_id'] ];
      return;
    }

    ob_start();

    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
    $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

    $kol = ( ! empty( $instance['kol'] ) ) ? absint( $instance['kol'] ) : 5;
    if ( ! $kol )
      $kol = 5;
    $cat = ( ! empty( $instance['cat'] ) ) ? absint( $instance['cat'] ) : 5;
    if ( ! $cat )
      $cat = array();


    $r = new WP_Query( apply_filters( 'widget_posts_args', array(
        'posts_per_page'      	=> $kol,
        'no_found_rows'       	=> true,
        'post_status'         	=> 'publish',
        'ignore_sticky_posts' 	=> true,
        'post_type'				=> 'cs_reviews'
    ) ) );

    if ($r->have_posts()) :
      ?>
      <?php echo $args['before_widget']; ?>
      <div class="reviews-list">
        <?php if ( $title ) : ?>
        <div class="widget-heading">
          <?php echo $args['before_title'] . $title . $args['after_title']; ?>
        </div>
        <?php endif; ?>
        <div class="widget-content">
        <?php while ( $r->have_posts() ) : $r->the_post(); ?>
          <div class="review-prev clearfix">
            <div class="review-content">
                <h3 class="review-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
              <div class="review-text">
                <?php \SweetCore\Helpers\CS_Functions::cs_excerpt('maxchar=100'); ?>
              </div>
            </div>
          </div>
        <?php endwhile; ?>
        </div>
        <div class="widget-meta">
          <a href="#">Все отзывы</a>
        </div>
      </div>
      <?php echo $args['after_widget']; ?>
      <?php
      wp_reset_postdata();

    endif;

    if ( ! $this->is_preview() ) {
      $cache[ $args['widget_id'] ] = ob_get_flush();
      wp_cache_set( 'widget_reviews', $cache, 'widget' );
    } else {
      ob_end_flush();
    }
  }

  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['kol'] = (int) $new_instance['kol'];
    $instance['cat'] = (int) $new_instance['cat'];
    $this->flush_widget_cache();

    $alloptions = wp_cache_get( 'alloptions', 'options' );
    if ( isset($alloptions['widget_cs_reviews_entries']) )
      delete_option('widget_cs_reviews_entries');

    return $instance;
  }

  /**
   * @access public
   */
  public function flush_widget_cache() {
    wp_cache_delete('widget_reviews', 'widget');
  }

  /**
   * @param array $instance
   */
  public function form( $instance ) {
    $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $kol    = isset( $instance['kol'] ) ? absint( $instance['kol'] ) : 5;
    $cat    = isset( $instance['cat'] ) ? absint( $instance['cat'] ) : 5;
    ?>
    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

    <p><label for="<?php echo $this->get_field_id( 'kol' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
      <input id="<?php echo $this->get_field_id( 'kol' ); ?>" name="<?php echo $this->get_field_name( 'kol' ); ?>" type="text" value="<?php echo $kol; ?>" size="3" /></p>
    <p><label for="<?php echo $this->get_field_id( 'cat' ); ?>"><?php _e( 'Category ID:' ); ?></label>
      <input id="<?php echo $this->get_field_id( 'cat' ); ?>" name="<?php echo $this->get_field_name( 'cat' ); ?>" type="text" value="<?php echo $cat; ?>" size="3" /></p>

    <?php
  }
}