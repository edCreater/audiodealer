<?php
class CS_Widget_Custom_Menu extends WP_Widget {

  public function __construct() {
    $widget_ops = array( 'description' => __('Add a custom menu to your sidebar.') );
    parent::__construct( 'cs_custom_menu', __('CS Custom Menu'), $widget_ops );
  }

  /**
   * @param array $args
   * @param array $instance
   */
  public function widget( $args, $instance ) {
    // Get menu
    $cust_menu = ! empty( $instance['cust_menu'] ) ? wp_get_nav_menu_object( $instance['cust_menu'] ) : false;

    if ( !$cust_menu )
      return;

    /** This filter is documented in wp-includes/default-widgets.php */
    $instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
    $custom_class = ( ! empty( $instance['custom_class'] ) ) ? $instance['custom_class'] : '';
    $link = ( ! empty( $instance['link'] ) ) ? $instance['link'] : '';


    echo $args['before_widget'];

    if ( !empty($instance['title']) )
      echo $args['before_title'] . $instance['title'] . $args['after_title'];

    $cust_menu_args = array(
        'fallback_cb' => '',
        'link_before' => '<span class="ico"></span>',
        'container_class' => $custom_class,
        'menu'        => $cust_menu
    );

    /**
     * Filter the arguments for the Custom Menu widget.
     *
     * @since 4.2.0
     *
     * @param array    $nav_menu_args {
     *     An array of arguments passed to wp_nav_menu() to retrieve a custom menu.
     *
     *     @type callback|bool $fallback_cb Callback to fire if the menu doesn't exist. Default empty.
     *     @type mixed         $menu        Menu ID, slug, or name.
     * }
     * @param stdClass $nav_menu      Nav menu object for the current menu.
     * @param array    $args          Display arguments for the current widget.
     */
    ?>
    <?php
    wp_nav_menu( apply_filters( 'widget_nav_menu_args', $cust_menu_args, $cust_menu, $args ) );

    echo $args['after_widget'];
  }
  /**
   * @param array $new_instance
   * @param array $old_instance
   * @return array
   */
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['link'] = strip_tags($new_instance['link']);

    if ( ! empty( $new_instance['title'] ) ) {
      $instance['title'] = htmlspecialchars($new_instance['title']);
    }
    if ( ! empty( $new_instance['custom_class'] ) ) {
      $instance['custom_class'] = htmlspecialchars($new_instance['custom_class']);
    }
    if ( ! empty( $new_instance['cust_menu'] ) ) {
      $instance['cust_menu'] = (int) $new_instance['cust_menu'];
    }
    return $instance;
  }

  /**
   * @param array $instance
   */
  public function form( $instance ) {
    $title = isset( $instance['title'] ) ? $instance['title'] : '';
    $custom_class = isset( $instance['custom_class'] ) ? $instance['custom_class'] : '';
    $cust_menu = isset( $instance['cust_menu'] ) ? $instance['cust_menu'] : '';
    $link     = isset( $instance['link'] ) ? esc_attr( $instance['link'] ) : '';

    // Get menus
    $menus = wp_get_nav_menus();

    // If no menus exists, direct the user to go and create some.
    ?>
    <p class="cust-menu-widget-no-menus-message" <?php if ( ! empty( $menus ) ) { echo ' style="display:none" '; } ?>>
      <?php
      if ( isset( $GLOBALS['wp_customize'] ) && $GLOBALS['wp_customize'] instanceof WP_Customize_Manager ) {
        // @todo When expanding a panel, the JS should be smart enough to collapse any existing panels and sections.
        $url = 'javascript: wp.customize.section.each(function( section ){ section.collapse(); }); wp.customize.panel( "nav_menus" ).focus();';
      } else {
        $url = admin_url( 'nav-menus.php' );
      }
      ?>
      <?php echo sprintf( __( 'No menus have been created yet. <a href="%s">Create some</a>.' ), esc_attr( $url ) ); ?>
    </p>
    <div class="cust-menu-widget-form-controls" <?php if ( empty( $menus ) ) { echo ' style="display:none" '; } ?>>
      <p><label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Link:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo $link; ?>" /></p>
      <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ) ?></label>
        <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>"/>
      </p>
      <p>
        <label for="<?php echo $this->get_field_id( 'custom_class' ); ?>"><?php _e( 'Custom Class:' ) ?></label>
        <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'custom_class' ); ?>" name="<?php echo $this->get_field_name( 'custom_class' ); ?>" value="<?php echo esc_attr( $custom_class ); ?>"/>
      </p>
      <p>
        <label for="<?php echo $this->get_field_id( 'cust_menu' ); ?>"><?php _e( 'Select Menu:' ); ?></label>
        <select id="<?php echo $this->get_field_id( 'cust_menu' ); ?>" name="<?php echo $this->get_field_name( 'cust_menu' ); ?>">
          <option value="0"><?php _e( '&mdash; Select &mdash;' ); ?></option>
          <?php foreach ( $menus as $menu ) : ?>
            <option value="<?php echo esc_attr( $menu->term_id ); ?>" <?php selected( $cust_menu, $menu->term_id ); ?>>
              <?php echo esc_html( $menu->name ); ?>
            </option>
          <?php endforeach; ?>
        </select>
      </p>
    </div>
    <?php
  }
}