<?php
/**
 * Custom Link widget class
 *
 * @since 2.8.0
 */
class CS_Widget_Calc_Link extends WP_Widget {

  public function __construct() {
    $widget_ops = array('classname' => 'widget_cs_calc_link_entries', 'description' => __( "Your site&#8217;s custom Link.") );
    parent::__construct('calc-link', __('Calculator Link'), $widget_ops);
    $this->alt_option_name = 'widget_cs_calc_link_entries';

    add_action( 'save_post', array($this, 'flush_widget_cache') );
    add_action( 'deleted_post', array($this, 'flush_widget_cache') );
    add_action( 'switch_theme', array($this, 'flush_widget_cache') );
  }

  /**
   * @param array $args
   * @param array $instance
   */
  public function widget( $args, $instance ) {
    $cache = array();
    if ( ! $this->is_preview() ) {
      $cache = wp_cache_get( 'widget_calc_link', 'widget' );
    }

    if ( ! is_array( $cache ) ) {
      $cache = array();
    }

    if ( ! isset( $args['widget_id'] ) ) {
      $args['widget_id'] = $this->id;
    }

    if ( isset( $cache[ $args['widget_id'] ] ) ) {
      echo $cache[ $args['widget_id'] ];
      return;
    }

    ob_start();

    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
    $link = ( ! empty( $instance['link'] ) ) ? $instance['link'] : '';
    $class = ( ! empty( $instance['class'] ) ) ? $instance['class'] : '';

    $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
    ?>
    <?php echo $args['before_widget']; ?>

    <div class="cs-calc-link-wrap">
      <a class="cs-calc-link <?php echo $class; ?>" href="<?php echo $link; ?>"><i class="ico"></i><span class="title"><?php echo $title; ?></span></a>
    </div>
    <?php echo $args['after_widget']; ?>
    <?php
    if ( ! $this->is_preview() ) {
      $cache[ $args['widget_id'] ] = ob_get_flush();
      wp_cache_set( 'widget_calc_link', $cache, 'widget' );
    } else {
      ob_end_flush();
    }
  }

  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['link'] = strip_tags($new_instance['link']);
    $instance['class'] = strip_tags($new_instance['class']);
    $this->flush_widget_cache();

    $alloptions = wp_cache_get( 'alloptions', 'options' );
    if ( isset($alloptions['widget_cs_calc_link_entries']) )
      delete_option('widget_cs_calc_link_entries');

    return $instance;
  }

  /**
   * @access public
   */
  public function flush_widget_cache() {
    wp_cache_delete('widget_calc_link', 'widget');
  }

  /**
   * @param array $instance
   */
  public function form( $instance ) {
    $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $link     = isset( $instance['link'] ) ? esc_attr( $instance['link'] ) : '';
    $class     = isset( $instance['class'] ) ? esc_attr( $instance['class'] ) : '';
    ?>
    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

    <p><label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Link:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo $link; ?>" /></p>

    <p><label for="<?php echo $this->get_field_id( 'class' ); ?>"><?php _e( 'Custom class:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'class' ); ?>" name="<?php echo $this->get_field_name( 'class' ); ?>" type="text" value="<?php echo $class; ?>" /></p>


    <?php
  }
}
