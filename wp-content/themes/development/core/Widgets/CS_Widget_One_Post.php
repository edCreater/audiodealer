<?php
class CS_Widget_One_Post extends WP_Widget
{
  public function __construct() {
    $widget_ops = array('classname' => 'widget_one_post_entries', 'description' => __( "Your site&#8217;s most recent Posts.") );
    parent::__construct('one-post', __('One Post'), $widget_ops);
    $this->alt_option_name = 'widget_one_post_entries';

    add_action( 'save_post', array($this, 'flush_widget_cache') );
    add_action( 'deleted_post', array($this, 'flush_widget_cache') );
    add_action( 'switch_theme', array($this, 'flush_widget_cache') );
  }

  public function widget($args, $instance) {
    $cache = array();
    if ( ! $this->is_preview() ) {
      $cache = wp_cache_get( 'widget_one_post', 'widget' );
    }

    if ( ! is_array( $cache ) ) {
      $cache = array();
    }

    if ( ! isset( $args['widget_id'] ) ) {
      $args['widget_id'] = $this->id;
    }

    if ( isset( $cache[ $args['widget_id'] ] ) ) {
      echo $cache[ $args['widget_id'] ];
      return;
    }

    ob_start();

    $link = ( ! empty( $instance['link'] ) ) ? $instance['link'] : '';

    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( '' );

    /** This filter is documented in wp-includes/default-widgets.php */
    $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
    $text = apply_filters( 'widget_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance );


    /**
     * Filter the arguments for the Recent Posts widget.
     *
     * @since 3.4.0
     *
     * @see WP_Query::get_posts()
     *
     * @param array $args An array of arguments used to retrieve the recent posts.
     */

    ?>
    <?php echo $args['before_widget']; ?>
    <?php if ( $title ) {
      //echo $args['before_title'] . $title . $args['after_title'];
    } ?>
    <div class="clearfix">
      <div class="cs-title">
        <?php if (!empty($link)) : ?>

          <h2><a href="<?php echo $link; ?>" title="<?php echo strip_tags(htmlspecialchars_decode($title)); ?>"><?php echo htmlspecialchars_decode($title); ?></a></h2>
        <?php else : ?>
          <h2><?php echo strip_tags(htmlspecialchars_decode($title)); ?></h2>
        <?php endif; ?>
      </div>
      <p><?php echo !empty( $instance['filter'] ) ? wpautop( $text ) : $text; ?></p>
      <div class="text-right">
        <?php if (!empty($link)) : ?>
          <a href="<?php echo $link; ?>" class="readmore">Читать далее...</a>
        <?php endif; ?>
      </div>
    </div>
    <?php echo $args['after_widget']; ?>
    <?php

    if ( ! $this->is_preview() ) {
      $cache[ $args['widget_id'] ] = ob_get_flush();
      wp_cache_set( 'widget_one_post', $cache, 'widget' );
    } else {
      ob_end_flush();
    }
  }

  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['link'] = strip_tags($new_instance['link']);
    $instance['title'] = htmlspecialchars($new_instance['title']);
    $instance['text'] =  $new_instance['text'];

    $this->flush_widget_cache();

    $alloptions = wp_cache_get( 'alloptions', 'options' );
    if ( isset($alloptions['widget_one_post_entries']) )
      delete_option('widget_one_post_entries');

    return $instance;
  }

  public function flush_widget_cache() {
    wp_cache_delete('widget_one_post', 'widget');
  }

  public function form( $instance ) {
    $link     = isset( $instance['link'] ) ? esc_attr( $instance['link'] ) : '';
    $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $text = esc_textarea($instance['text']);
    ?>
    <p><label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Link:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo $link; ?>" /></p>

    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

    <textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea></p>

    <?php
  }
}
