<?php
namespace SweetCore\Helpers;

class CS_Options {

	private $settings_api;

	function __construct() {
		$this->settings_api = new \SweetCore\Libs\WeDevs_Settings_API;

		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );

	}

	function admin_init() {

		//set the settings
		$this->settings_api->set_sections( $this->get_settings_sections() );
		$this->settings_api->set_fields( $this->get_settings_fields() );

		//initialize settings
		$this->settings_api->admin_init();
	}

	function admin_menu() {
		//add_options_page( 'Settings API', 'Settings API', 'delete_posts', 'settings_api_test', array($this, 'plugin_page') );
		add_menu_page( 'CS Settings', 'CS Settings', 'delete_posts', 'cs_theme_options', array(
				$this,
				'plugin_page'
		), '', "99.3" );
		//add_submenu_page( 'options.php', 'CS Settings', 'CS Settings', 'delete_posts', 'cs_theme_options', array($this, 'plugin_page') );
	}

	function cs_list_pages() {
		global $wpdb;

		$array = array();
		$pages = get_pages();
		if ( $pages ) {
			foreach ( $pages as $page ) {
				$array[ $page->ID ] = $page->post_title;
			}
		}

		return $array;
	}

	function cs_list_categories() {
		global $wpdb;

		$array      = array();
		$categories = get_categories( array( 'hide_empty' => false ) );
		if ( $categories ) {
			foreach ( $categories as $category ) {
				$array[ $category->term_id ] = $category->name;
			}
		}

		return $array;
	}

	function get_settings_sections() {
		$sections = array(
				array(
						'id'    => 'cs_basics',
						'title' => __( 'Basic Settings', 'cs' )
				),
				array(
						'id'    => 'cs_home',
						'title' => __( 'Home Settings', 'cs' )
				),
				array(
						'id'    => 'cs_contacts',
						'title' => __( 'Контакты', 'cs' )
				),
				array(
						'id'    => 'cs_others',
						'title' => __( 'Other Settings', 'cs' )
				)
		);

		return $sections;
	}

	/**
	 * Returns all the settings fields
	 *
	 * @return array settings fields
	 */
	function get_settings_fields() {
		$pages           = $this->cs_list_pages();
		$categories      = $this->cs_list_categories();
		$settings_fields = array(
				'cs_basics'   => array(
						array(
								'name'    => 'cs_slogan',
								'label'   => __( 'Слоган', 'cs' ),
								'desc'    => __( 'Слоган в шапке', 'cs' ),
								'size'    => '100%',
								'type'    => 'textarea',
								'default' => '',
						),
						array(
								'name'    => 'cs_footertext',
								'label'   => __( 'Текст в футере', 'cs' ),
								'desc'    => __( 'Текст в футере (4 колонка)', 'cs' ),
								'teeny'   => false,
								'size'    => '100%',
								'type'    => 'wysiwyg',
								'default' => '',
						),
						array(
								'name'    => 'cs_copy',
								'label'   => __( 'Копирайт', 'cs' ),
								'desc'    => __( 'Копирайт', 'cs' ),
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_scripts',
								'label'   => __( 'Скрипты в футере', 'cs' ),
								'desc'    => __( 'Скрипты в футере', 'cs' ),
								'size'    => '100%',
								'type'    => 'textarea',
								'default' => '',
						),

				),
				'cs_home'   => array(
						array(
								'name'    => 'cs_video',
								'label'   => __( 'Видео', 'cs' ),
								'desc'    => __( 'Видео (ифрейм)', 'cs' ),
								'size'    => '100%',
								'type'    => 'textarea',
								'default' => '',
						),
						array(
								'name'    => 'cs_cbhead_1',
								'label'   => __( 'Каллбэк заголовок 1', 'cs' ),
								'desc'    => __( 'Каллбэк заголовок 1', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_cbhead_2',
								'label'   => __( 'Каллбэк заголовок 2', 'cs' ),
								'desc'    => __( 'Каллбэк заголовок 2', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_cbsubhead_1',
								'label'   => __( 'Каллбэк подзаголовок 1', 'cs' ),
								'desc'    => __( 'Каллбэк подзаголовок 1', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_cbsubhead_2',
								'label'   => __( 'Каллбэк подзаголовок 2', 'cs' ),
								'desc'    => __( 'Каллбэк подзаголовок 2', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_bhead_1',
								'label'   => __( 'Преимущества заголовок 1', 'cs' ),
								'desc'    => __( 'Преимущества заголовок 1', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_bhead_2',
								'label'   => __( 'Преимущества заголовок 2', 'cs' ),
								'desc'    => __( 'Преимущества заголовок 2', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_elhead_1',
								'label'   => __( 'Элементы заголовок 1', 'cs' ),
								'desc'    => __( 'Элементы заголовок 1', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_elhead_2',
								'label'   => __( 'Элементы заголовок 2', 'cs' ),
								'desc'    => __( 'Элементы заголовок 2', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_elsubhead',
								'label'   => __( 'Элементы подзаголовок', 'cs' ),
								'desc'    => __( 'Элементы подзаголовок', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_cathead',
								'label'   => __( 'Образы заголовок', 'cs' ),
								'desc'    => __( 'Образы заголовок', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_teamhead',
								'label'   => __( 'Команда заголовок', 'cs' ),
								'desc'    => __( 'Команда заголовок', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_markethead',
								'label'   => __( 'Магазин заголовок', 'cs' ),
								'desc'    => __( 'Магазин заголовок', 'cs' ),
								'size'    => '100%',
								'type'    => 'text',
								'default' => '',
						),
				),
				'cs_contacts' => array(
						array(
								'name'    => 'cs_phone',
								'label'   => __( 'Телефон', 'cs' ),
								'desc'    => __( 'Телефон', 'cs' ),
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_address',
								'label'   => __( 'Адрес', 'cs' ),
								'desc'    => __( 'Адрес', 'cs' ),
								'size'    => '100%',
								'type'    => 'textarea',
								'default' => '',
						),

						array(
								'name'    => 'cs_vk',
								'label'   => __( 'Вконтакте', 'cs' ),
								'desc'    => __( 'Вконтакте ссылка', 'cs' ),
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_fb',
								'label'   => __( 'Фейсбук', 'cs' ),
								'desc'    => __( 'Фейсбук ссылка', 'cs' ),
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_ok',
								'label'   => __( 'Одноклассники', 'cs' ),
								'desc'    => __( 'Одноклассники ссылка', 'cs' ),
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_vk_widget',
								'label'   => __( 'Vkontakte Widget', 'cs' ),
								'desc'    => __( 'Vkontakte Widget', 'cs' ),
								'type'    => 'textarea',
								'default' => '',
						),
						array(
								'name'    => 'cs_ok_widget',
								'label'   => __( 'Odnoklassniki Widget', 'cs' ),
								'desc'    => __( 'Odnoklassniki Widget', 'cs' ),
								'type'    => 'textarea',
								'default' => '',
						),

				),
				'cs_others'   => array(
						array(
								'name'    => 'cs_404_title',
								'label'   => __( '404 title', 'cs' ),
								'desc'    => __( '404 title', 'cs' ),
								'type'    => 'text',
								'default' => '',
						),
						array(
								'name'    => 'cs_404_content',
								'label'   => __( '404 title', 'cs' ),
								'desc'    => __( '404 title', 'cs' ),
								'size'    => '100%',
								'teeny'   => false,
								'type'    => 'wysiwyg',
								'default' => '',
						),
						array(
								'name'    => 'cs_documents_cat',
								'label'   => __( 'Рубрика документов', 'cs' ),
								'desc'    => __( 'Рубрика документов', 'cs' ),
								'type'    => 'select',
								'options' => $categories
						),
						array(
								'name'    => 'cs_news_cat',
								'label'   => __( 'Рубрика новостей', 'cs' ),
								'desc'    => __( 'Рубрика новостей', 'cs' ),
								'type'    => 'select',
								'options' => $categories
						),
				)
		);

		return $settings_fields;
	}

	function plugin_page() {
		echo '<div class="wrap">';

		$this->settings_api->show_navigation();
		$this->settings_api->show_forms();

		echo '</div>';
	}


}

