<?php

/**
 * Created by Ed.Creater <ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 03.03.2016
 */
namespace SweetCore\Helpers;

class CS_Seo
{

  private $custom_meta_fields;

  function __construct()
  {

    add_action('add_meta_boxes', array($this, 'cs_seo_meta_box'), 10, 1);
    add_action("save_post", array($this, "save_seo_meta_box"), 10, 3);

    // Category
    add_action('category_add_form_fields', array($this, 'category_seobox_add'), 100, 1);
    add_action('category_edit_form_fields', array($this, 'category_seobox_edit'), 100, 1);

    // Catalog
    add_action('catalog_category_add_form_fields', array($this, 'category_seobox_add'), 100, 1);
    add_action('catalog_category_edit_form_fields', array($this, 'category_seobox_edit'), 100, 1);

    // Category
    add_action('created_category', array($this, 'save_category_seodata'), 10, 1);
    add_action('edited_category', array($this, 'save_category_seodata'), 10, 1);

    // Catalog
    add_action('created_catalog_category', array($this, 'save_category_seodata'), 10, 1);
    add_action('edited_catalog_category', array($this, 'save_category_seodata'), 10, 1);

  }

  /**
   * Display CS Meta Title
   * @return print Title
   */
  public static function cs_meta_title()
  {
    global $wp_query, $post;
    $out = '';
    $basics = get_option('cs_basics');

    if (is_single() || is_page()) {

      $out = get_post_meta($post->ID, 'cs-seotitle', true);

      if (empty($out)) {
        $out = get_the_title($post->ID);
      }
    }
    if (is_404()) {
      $out = 'Страница не найдена';
    }
    if (is_search()) {
      $out = 'Результаты поиска';
    }
    if (is_tax()) {
      $term_slug = get_query_var('term');
      $tax_name = get_query_var('taxonomy');
      $term = get_term_by('slug', $term_slug, $tax_name);

      $out = get_term_meta($term->term_id, 'cs-seotitle', true);

      if (empty($out)) {
        $out = $term->name;
      }
    }
    if (is_category()) {

      $cat_id = get_query_var('cat');
      $cat = get_category($cat_id);
      $out = get_term_meta($cat_id, 'cs-seotitle', true);

      if (empty($out)) {
        $out = $cat->name;
      }
    }
    if (is_home()) {

      if (isset($basics['cs_home_title'])) {
        $out = $basics['cs_home_title'];
      } else {
        $out = get_bloginfo('sitename');
      }
    }

    return print('<title>' . $out . '</title>' . "\r\n");
  }

  /**
   * Display CS Meta Fields (description keywords)
   * @return print description
   */

  public static function cs_meta_field()
  {
    global $wp_query, $post;
    $out = '';
    $basics = get_option('cs_basics');

    if (is_single() || is_page()) {
      $descr = get_post_meta($post->ID, 'cs-seodescr', true);
      if ($descr) {
        $out .= '<meta name="description" lang="ru" content="' . $descr . '" />' . "\r\n";
      } else {
        $out .= '<meta name="description" lang="ru" content="' . $post->post_title . '" />' . "\r\n";
      }
    }
    if (is_tax()) {
      $term_slug = get_query_var('term');
      $tax_name = get_query_var('taxonomy');
      $term = get_term_by('slug', $term_slug, $tax_name);

      $descr = get_term_meta($term->term_id, 'cs-seodescr', true);

      if ($descr) {
        $out .= '<meta name="description" lang="ru" content="' . $descr . '" />' . "\r\n";
      } else {
        $out .= '<meta name="description" lang="ru" content="' . $term->name . '" />' . "\r\n";
      }
    }
    if (is_category()) {

      $cat_id = get_query_var('cat');
      $cat = get_category($cat_id);

      $descr = get_term_meta($cat_id, 'cs-seodescr', true);

      if ($descr) {
        $out .= '<meta name="description" lang="ru" content="' . $descr . '" />' . "\r\n";
      } else {
        $out .= '<meta name="description" lang="ru" content="' . $cat->name . '" />' . "\r\n";
      }
    }

    if (is_home()) {
      if (isset($basics['cs_home_description'])) {
        $out .= '<meta name="description" lang="ru" content="' . $basics['cs_home_description'] . '" />' . "\r\n";
      } else {
        $out .= '<meta name="description" lang="ru" content="' . get_bloginfo('description') . '" />' . "\r\n";
      }
    }

    return print($out);
  }

  /**
   * Display Open Graph Schema
   * @return print opengraph schema
   */

  public static function cs_og()
  {
    global $wp_query, $post;
    $out = '<meta property="og:type" content="article" />' . "\n";

    if (is_single() || is_page()) {

      $title = get_post_meta($post->ID, 'cs-seotitle', true);
      $descr = get_post_meta($post->ID, 'cs-seodescr', true);

      //title
      if ($title) {
        $out .= '<meta property="og:title" content="' . $title . '" />' . "\n";
      } else {
        $out .= '<meta property="og:title" content="' . $post->post_title . '" />' . "\n";
      }
      //description
      if ($descr) {
        $out .= '<meta property="og:description" content="' . $descr . '" />' . "\n";
      } else {
        $out .= '<meta property="og:description" content="' . $post->post_title . '" />' . "\n";
      }
      //url
      $out .= '<meta property="og:url" content="' . get_the_permalink($post) . '" />' . "\n";
      //image
      if (has_post_thumbnail($post->ID)) {
        $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'post-thumbnail');

        if ($thumbnail[0])
          $out .= '<meta property="og:image" content="' . $thumbnail[0] . '" />' . "\n";
      } elseif (is_front_page()) {
        $out .= '<meta property="og:image" content="' . THEME_DIR_URI . '/img/logo.png" />' . "\n";
      }
    }
    if (is_tax()) {

      $term_slug = get_query_var('term');
      $tax_name = get_query_var('taxonomy');
      $term = get_term_by('slug', $term_slug, $tax_name);

      $title = get_term_meta($term->term_id, 'cs-seotitle', true);
      $descr = get_term_meta($term->term_id, 'cs-seodescr', true);

      //title
      if ($title) {
        $out .= '<meta property="og:title" content="' . $title . '" />' . "\n";
      } else {
        $out .= '<meta property="og:title" content="' . $term->name . '" />' . "\n";
      }
      //description
      if ($descr) {
        $out .= '<meta property="og:description" content="' . $descr . '" />' . "\n";
      } else {
        $out .= '<meta property="og:description" content="' . $term->name . '" />' . "\n";
      }
      //url
      $out .= '<meta property="og:url" content="' . get_category_link($term->term_id) . '" />' . "\n";

    }

    return print_r($out);
  }


  /**
   * Insert SEO Meta Box
   * return void
   */
  function cs_seo_meta_box()
  {

    add_meta_box("seo-meta-box", "Seo Meta Box", array($this, "seo_meta_box_markup"), array('post', 'page', 'cs_catalog'), "normal", "default", null);

  }

  function seo_meta_box_markup($object)
  {
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
      <style>
        .seobox input,
        .seobox textarea{
          width: 100%;
        }
      </style>
    <table class="form-table seobox">
      <tr>
        <th><label for="cs-seotitle">Title</label></th>
        <td>
          <input name="cs-seotitle" type="text" value="<?php echo get_post_meta($object->ID, "cs-seotitle", true); ?>" size="30">
        </td>
      </tr>
      <tr>
        <th><label for="cs-seodescr">Description</label></th>
        <td>
          <textarea name="cs-seodescr" cols="60" rows="4"><?php echo get_post_meta($object->ID, "cs-seodescr", true); ?></textarea>
        </td>
      </tr>
    </table>
    <?php
  }

  function save_seo_meta_box($post_id, $post, $update)
  {
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
      return $post_id;

    if (!current_user_can("edit_post", $post_id))
      return $post_id;

    if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
      return $post_id;

    $slug = array('post', 'page', 'cs_news');
    if (!in_array( $post->post_type, $slug ))
      return $post_id;

    $cs_seotitle_value = "";
    $cs_seodescr_value = "";

    if (isset($_POST["cs-seotitle"])) {
      $cs_seotitle_value = $_POST["cs-seotitle"];
    }
    update_post_meta($post_id, "cs-seotitle", $cs_seotitle_value);

    if (isset($_POST["cs-seodescr"])) {
      $cs_seodescr_value = $_POST["cs-seodescr"];
    }
    update_post_meta($post_id, "cs-seodescr", $cs_seodescr_value);

  }


  // Taxonomy
  function category_seobox_add($tag)
  { ?>
    <h3>SEO box</h3>
    <div class="form-field">
      <label for="cs-seotitle"><?php _e('SEO Title') ?></label>
      <input name="cs-seotitle" id="cs-seotitle" type="text" value="" size="40" aria-required="true"/>
    </div>
    <div class="form-field">
      <label for="cs-seodescr"><?php _e('SEO Description') ?></label>
      <input name="cs-seodescr" id="cs-seodescr" type="text" value="" size="40" aria-required="true"/>
    </div>

  <?php }


  function category_seobox_edit($tag)
  { ?>

    <table class="form-table">
      <tr>
        <th>SEO box</th>
      </tr>
      <tr class="form-field">
        <th scope="row" valign="top">
          <label for="cs-seotitle"><?php _e('SEO Title'); ?></label>
        </th>
        <td>
          <input name="cs-seotitle" id="cs-seotitle" type="text"
                 value="<?php echo get_term_meta($tag->term_id, 'cs-seotitle', true); ?>" size="40"
                 aria-required="true"/>
        </td>
      </tr>
      <tr class="form-field">
        <th scope="row" valign="top">
          <label for="cs-seodescr"><?php _e('SEO Description'); ?></label>
        </th>
        <td>
          <input name="cs-seodescr" id="cs-seodescr" type="text"
                 value="<?php echo get_term_meta($tag->term_id, 'cs-seodescr', true); ?>" size="40"
                 aria-required="true"/>
        </td>
      </tr>

    </table>
  <?php }


  function save_category_seodata($term_id)
  {

    if (isset($_POST['cs-seotitle']))
      update_term_meta($term_id, 'cs-seotitle', $_POST['cs-seotitle']);

    if (isset($_POST['cs-seodescr']))
      update_term_meta($term_id, 'cs-seodescr', $_POST['cs-seodescr']);


  }

}