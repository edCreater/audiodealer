<?php

/**
 * Created by Ed.Creater <ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 04.04.2016
 */
namespace SweetCore\Helpers;

class CS_WP_Upgrade {

	function __construct() {

		$this->disable_rest_api();
		$this->disable_rss();
		$this->remove_links();
		$this->cs_remove_wp_scripts();

	}

	private function disable_rest_api() {

		add_filter( 'json_enabled', '__return_false' );
		add_filter( 'json_jsonp_enabled', '__return_false' );


		// Отключаем сам REST API
		add_filter( 'rest_enabled', '__return_false' );

		// Отключаем фильтры REST API
		remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10, 0 );
		remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
		remove_action( 'auth_cookie_malformed', 'rest_cookie_collect_status' );
		remove_action( 'auth_cookie_expired', 'rest_cookie_collect_status' );
		remove_action( 'auth_cookie_bad_username', 'rest_cookie_collect_status' );
		remove_action( 'auth_cookie_bad_hash', 'rest_cookie_collect_status' );
		remove_action( 'auth_cookie_valid', 'rest_cookie_collect_status' );
		remove_filter( 'rest_authentication_errors', 'rest_cookie_check_errors', 100 );

		// Отключаем события REST API
		remove_action( 'init', 'rest_api_init' );
		remove_action( 'rest_api_init', 'rest_api_default_filters', 10, 1 );
		remove_action( 'parse_request', 'rest_api_loaded' );

		// Отключаем Embeds связанные с REST API
		remove_action( 'rest_api_init', 'wp_oembed_register_route' );
		remove_filter( 'rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10, 4 );

		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
		// если собираетесь выводить вставки из других сайтов на своем, то закомментируйте след. строку.
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );

		add_action( 'template_redirect', function () {
			if ( preg_match( '#^/wp-json/(.*)#', $_SERVER['REQUEST_URI'] ) ) {
				wp_redirect( get_option( 'siteurl' ), 301 );
				die();
			}
		} );

	}

	private function remove_links() {

		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'rsd_link' );

		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );

	}

	private function disable_rss() {

		function mysite_kill_feed() {
			wp_die( __( 'No feeds available, please visit our <a href="' . get_bloginfo( 'url' ) . '">homepage</a>!' ) );
		}

		add_action( 'do_feed', 'mysite_kill_feed', 1 );
		add_action( 'do_feed_rdf', 'mysite_kill_feed', 1 );
		add_action( 'do_feed_rss', 'mysite_kill_feed', 1 );
		add_action( 'do_feed_rss2', 'mysite_kill_feed', 1 );
		add_action( 'do_feed_atom', 'mysite_kill_feed', 1 );
	}

	function cs_remove_wp_scripts() {

		if ( ! current_user_can( 'edit_published_posts' ) ) {
			add_action( 'wp_print_styles', 'my_deregister_styles', 100 );

			function my_deregister_styles() {
				//wp_deregister_style( 'amethyst-dashicons-style' );
				wp_deregister_style( 'dashicons' );
				wp_deregister_style( 'open-sans' );
				show_admin_bar( false );
			}
		}

		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );

	}


}