<?php

/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 01.08.2016
 */
namespace SweetCore\Shortcodes;

class CS_Benefits_Shortcode {

  public $params;

  function __construct() {

    add_shortcode( 'cs_benefits', array( $this, 'cs_benefits_shortcode' ) );

  }

  function cs_benefits_shortcode( $atts, $content = null ) {

    ob_start();
    ?>
    <div class="row benefit-row">

    <?php $args = array('post_type' => 'cs_benefits'); ?>
    <?php $benefits = new \WP_Query($args); ?>
    <?php $i = 0; ?>
    <?php while ($benefits->have_posts()) : $benefits->the_post(); ?>
      <?php $i++; ?>

      <div class="benefit col-sm-12">
        <div class="icon">
          <?php the_post_thumbnail('full'); ?>
        </div>
        <div class="content">
          <p class="title"><?php the_title(); ?></p>
          <p class="text"><?php the_content(); ?></p>
        </div>
      </div>
      <?php if ((($i % 2) == 0) && ($i == $benefits->post_count)) { ?>
        </div>
      <?php } elseif ((($i % 2) == 0) && ($i <> $benefits->post_count)) { ?>
        </div>
        <div class="row benefit-row">
      <? }; ?>
    <?php endwhile; ?>

    </div>
    <?
    $output_string=ob_get_contents();
    ob_end_clean();

    return $output_string;
  }


}