<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 04.08.2015
 */
namespace SweetCore\PostTypes;

class CS_Portfolio
{
	function __construct(){

		$this->cs_portfolio_posttype();
		$this->cs_portfolio_category();

	}

	function cs_portfolio_posttype() {

		register_post_type('cs_portfolio', array(
				'label'             => 'Портфолио',
				'public'            => true,
				'show_ui'           => true,
				'show_in_nav_menus' => true,
				'rewrite'           => array(
					'slug'          => 'cs_portfolio',
				),
				'hierarchical'      => false,
				'has_archive'       => true,
				'supports'          => array(
					'title', 'post-formats', 'page-attributes', 'editor', 'thumbnail', 'tags'
				),
			)
		);
	}

	function cs_portfolio_category() {

		$labels = array(
			'name' => _x( 'Portfolio categories', 'taxonomy general name' ),
			'singular_name' => _x( 'Portfolio category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Portfolio categories' ),
			'popular_items' => __( 'Popular Portfolio categories' ),
			'all_items' => __( 'All Portfolio categories' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Portfolio category' ),
			'update_item' => __( 'Update Portfolio category' ),
			'add_new_item' => __( 'Add New Portfolio category' ),
			'new_item_name' => __( 'New Portfolio category' ),
			'separate_items_with_commas' => __( 'Separate Portfolio categories with commas' ),
			'add_or_remove_items' => __( 'Add or remove Portfolio categories' ),
			'choose_from_most_used' => __( 'Choose from the most used Portfolio categories' ),
			'menu_name' => __( 'Portfolio categories' ),
		);
		$args = array(
			'labels'=>$labels,
			'hierarchical' => true,
		);
		register_taxonomy( 'portfolio_category', 'cs_portfolio', $args );

	}
}