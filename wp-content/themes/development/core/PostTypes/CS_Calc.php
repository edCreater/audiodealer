<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 04.08.2015
 */
namespace SweetCore\PostTypes;

class CS_Calc
{
  function __construct(){

    $this->cs_calc_posttype();

  }

  function cs_calc_posttype() {

    register_post_type('cs_benefits', array(
            'label'             => 'Калькулятор',
            'public'            => true,
            'show_ui'           => true,
            'show_in_nav_menus' => false,
            'hierarchical'      => false,
            'has_archive'       => false,
            'rewrite'           => false,
            'supports'          => array(
                'title', 'editor', 'thumbnail'
            ),
        )
    );
  }

}