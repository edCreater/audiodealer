<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 04.08.2015
 */
namespace SweetCore\PostTypes;

class CS_Tax_Brand
{
	function __construct(){

		$this->cs_tax_brand_register();

	}

	function cs_tax_brand_register() {


			$labels = array(
					'name' => _x( 'Brands', 'taxonomy general name' ),
					'singular_name' => _x( 'Brand', 'taxonomy singular name' ),
					'search_items' =>  __( 'Search Brands' ),
					'all_items' => __( 'All Brands' ),
					'parent_item' => __( 'Parent Brand' ),
					'parent_item_colon' => __( 'Parent Brands:' ),
					'edit_item' => __( 'Edit Brands' ),
					'update_item' => __( 'Update Brands' ),
					'add_new_item' => __( 'Add New Brand' ),
					'new_item_name' => __( 'New Brand Name' ),
					'menu_name' => __( 'Brands' ),
			);

			register_taxonomy("product_brand",
					array("product"),
					array(
							'hierarchical' => true,
							'labels' => $labels,
							'show_ui' => true,
							'query_var' => true,
							'rewrite' => array( 'slug' => 'brands', 'with_front' => true ),
							'show_admin_column' => true
					)
			);

		}
}