<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 04.08.2015
 */
namespace SweetCore\PostTypes;

class CS_Clients
{
  function __construct(){

    $this->cs_clients_posttype();

  }

  function cs_clients_posttype() {

    register_post_type('cs_clients', array(
            'label'             => __('Клиенты', 'cs'),
            'public'            => true,
            'show_ui'           => true,
            'show_in_nav_menus' => true,
            'rewrite'           => array(
                'slug' => 'team',
            ),
            'hierarchical'      => false,
            'has_archive'       => false,
            'supports'          => array(
                'title', 'post-formats', 'page-attributes', 'editor', 'thumbnail', 'custom-fields'
            ),
        )
    );
  }
}