<?php
/**
 * Plugin Name: SweetCore
 * Description: Setup SweetCore.
 * Version: 1.0.0
 * License: A "Slug" license name e.g. GPL2
 */


if (!class_exists('CS_Core')) {

  define("THEME_DIR", get_template_directory());
  define("THEME_DIR_URI", get_template_directory_uri());

  define('SWEETCORE_DIR', THEME_DIR . '/core');
  define('SWEETCORE_ADMIN_DIR', THEME_DIR . '/core/admin');

  if (file_exists(SWEETCORE_DIR . '/CS_Core.php')) {
    require_once(SWEETCORE_DIR . '/CS_Core.php');
  }

}
