<?php
/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 25.10.2016
 */
?>
<?php get_header(); ?>
<?php while(have_posts()) : the_post(); ?>
<div class="page__container">
	<div class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-sm-24">
					<?php \SweetCore\Helpers\CS_Functions::cs_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="page__heading">
		<div class="container">
			<div class="row">
				<div class="col-sm-24">
					<h1 class="page__title"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="page__content">
		<div class="container">
			<div class="row">
				<div class="col-sm-24">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>
