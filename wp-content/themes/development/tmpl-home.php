<?php
/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 22.10.2016
 * Template Name: Home
 */
?>
<?php get_header(); ?>
	<div class="products__section">
		<div class="container products-container">
			<div class="row">
				<div class="col-sm-24 products__section_heading">
					<div class="products__slider_controls slider__controls">
						<a href="#" class="products__slider_control slider__control prev">&nbsp;</a> <span><span
									class="current-slide-num">1</span> / <span class="total-slides-num">5</span></span>
						<a href="#" class="products__slider_control slider__control next">&nbsp;</a>
					</div>
					<h2 class='section__title'>Новинки</h2>
				</div>
			</div>
			<div class="products row">
				<?php
				$args = array(
						'post_type'           => 'product',
						'post_status'         => 'publish',
						'ignore_sticky_posts' => 1
				);
				$products   = new WP_Query( $args );

				if ( $products->have_posts() ) : ?>
				<div class="products__slider swiper-container">
					<div class="swiper-wrapper">
						<?php while ($products->have_posts()) : $products->the_post(); ?>
							<div class="swiper-slide products__slide">
								<?php wc_get_template_part( 'content', 'product' ); ?>
							</div>
						<?php endwhile; ?>
					</div>
					<div class="slider__next"></div>
					<div class="slider__prev"></div>
				</div>

			</div><!-- Products.end -->
			<?php
			endif;
			wp_reset_query();
			?>
		</div><!-- ProductsContainer.end -->
	</div>

	<div class="brands__section">
		<div class="container brands__container">
			<?php
			$brands_list =  get_terms('product_brand', array(
					'hide_empty' => false,
			)); ?>
			<ul class="brands__list">
			<?php foreach ( $brands_list as $brand ) {
				$brand_image_src_term = get_tax_meta($brand->term_id, 'cs_image_brand_thumb');
				$brand_image_src = $brand_image_src_term['src']; ?>
					<li class="brand__item">
						<a href="<?php echo get_term_link( $brand->slug, 'product_brand' ) ?>">
							<img src="<?php echo $brand_image_src; ?>" alt="<?php echo $brand->name; ?>" />
						</a>
					</li>
			<?php } ?>
			</ul>
		</div>
	</div>

<div class="projects__section">
	<div class="container projects__container">
		<div class="row">
			<div class="col-sm-24 projects__section_heading no-padding">
				<div class="projects__slider_controls slider__controls">
					<a href="#" class="projects__slider_control slider__control prev">&nbsp;</a> <span><span
								class="current-slide-num">1</span> / <span class="total-slides-num">5</span></span>
					<a href="#" class="projects__slider_control slider__control next">&nbsp;</a>
				</div>
				<h2 class='section__title'>Наши проекты</h2>
			</div>
		</div>
		<div class="projects row">
			<?php $args = array(
					'post_type' => 'cs_projects',
					'posts_per_page' => 4
			);
			$projects = new WP_Query($args);
			if ($projects->have_posts()) : ?>
			<div class="projects__slider swiper-container">
				<div class="swiper-wrapper">
				<?php while ($projects->have_posts()) : $projects->the_post(); ?>
					<div class="projects__slide swiper-slide">
						<div class="project-img">
							<?php the_post_thumbnail('shop_catalog', array('class'=>'img-responsive')); ?>
						</div>
						<div class="project__info">
							<div class="project__name">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
							</div>
							<div class="project__date">
								<?php echo get_the_date('dd.mm.YY'); ?>
							</div>
							<div class="project__description">
								<?php the_excerpt(); ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>