<?php
/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 24.09.2016
 */


require_once get_template_directory() . '/core/sweetcore.php';

if( !function_exists( 'mtranse_styles' ) ) {

  function mtranse_styles() {

  	/** Fonts */
	  wp_register_style( 'pt_sans', 'https://fonts.googleapis.com/css?family=PT+Sans', array(), '', 'all' );

    /** Hamburgers **/
    wp_register_style( 'hamburgers', THEME_DIR_URI . '/assets/libs/hamburgers/hamburgers.min.css', array(), '', 'all' );
    wp_enqueue_style( 'hamburgers' );

    /** Swiper **/
    wp_register_style( 'swiper', THEME_DIR_URI . '/assets/libs/swiper/css/swiper.min.css', array(), '', 'all' );
    wp_enqueue_style( 'swiper' );

    /** Bootstrap Style **/
    wp_register_style( 'bootstrap', THEME_DIR_URI . '/assets/css/commons.css', array(), '', 'all' );
    wp_enqueue_style( 'bootstrap' );

    /** Default Style **/
    wp_register_style( 'default', THEME_DIR_URI . '/assets/css/default.css', array(), '', 'all' );
    wp_enqueue_style( 'default' );

  }
  add_action('wp_enqueue_scripts', 'mtranse_styles');

}

if( !function_exists( 'audio_scripts' ) ) {
  function audio_scripts() {

    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-slider');

    wp_register_script('bootstrap', THEME_DIR_URI . '/assets/libs/bootstrap-4/js/bootstrap.min.js', array('jquery'), '', true);
    wp_enqueue_script('bootstrap');

    wp_register_script('swiper', THEME_DIR_URI . '/assets/libs/swiper/js/swiper.min.js', array('jquery'), '', true);
    wp_enqueue_script('swiper');

    wp_register_script('default', THEME_DIR_URI . '/assets/js/default.js', array('jquery'), '', true);
    wp_enqueue_script('default');

    wp_localize_script( 'default', 'myajax',
        array(
            'url'   => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('myajax-nonce')
        ));

  }
  add_action('wp_enqueue_scripts', 'audio_scripts');
}

add_theme_support( 'post-formats', array( 'image' ) );


function wpb_mce_buttons_2($buttons) {
  array_unshift($buttons, 'styleselect');
  return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

  $style_formats = array(
      array(
          'title' => 'Table bordered',
          'block' => 'table',
          'classes' => 'table table-bordered',
          'wrapper' => true,

      ),
      array(
          'title' => 'Blue Button',
          'block' => 'span',
          'classes' => 'primary-button-link',
          'wrapper' => true
      ),

  );
  // Insert the array, JSON ENCODED, into 'style_formats'
  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

function wpdocs_theme_add_editor_styles() {
  add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );