var gulp = require('gulp');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var prefixer    = require('gulp-autoprefixer');
var watch    = require('gulp-watch');
var uglify    = require('gulp-uglify');
var sass = require('gulp-sass');
var rigger = require('gulp-rigger');
var cssmin = require('gulp-minify-css');
var livereload = require('gulp-livereload');
var changed  = require('gulp-changed');

var path = {
  public: {
    php: 'audio/',
    js: 'audio/assets/js/',
    css: 'audio/assets/css/',
    img: 'audio/assets/img/'
  },
  private: {
    php: 'development/**/*.php',
    js: 'development/js/default.js',
    css: 'development/sass/*.scss',
    img: 'development/img/**/*.*'
  },
  watch: {
    php: 'development/**/*.php',
    js: 'development/js/**/*.js',
    css: 'development/sass/**/*.scss',
    img: 'development/img/**/*.*'
  }
};


var plumberErrorHandler = { errorHandler: notify.onError({

  title: 'Gulp',
  message: 'Error: <%= error.message %>'

})

};

gulp.task('php:build', function(){

  gulp.src(path.private.php)
      .pipe(changed(path.public.php))
      .pipe(gulp.dest(path.public.php))
      .pipe(livereload());


});

gulp.task('js:build', function () {

  gulp.src(path.private.js)
      .pipe(plumber())
      .pipe(rigger())
      .pipe(uglify())
      .pipe(gulp.dest(path.public.js))
      .pipe(livereload());

});

gulp.task('css:build', function () {

  gulp.src(path.private.css)
      .pipe(plumber(plumberErrorHandler))
      .pipe(sass())
      .pipe(prefixer({ browsers: ['last 15 versions'], cascade: false }))
      .pipe(cssmin())
      .pipe(changed(path.public.css))
      .pipe(gulp.dest(path.public.css))
      .pipe(livereload());

});

gulp.task('img:build', function () {

  gulp.src(path.private.img)
      .pipe(changed(path.public.img))
      .pipe(gulp.dest(path.public.img))
      .pipe(livereload());

});

gulp.task('build', [
    'js:build',
    'css:build',
    'img:build'
]);

gulp.task('watch', function() {

  livereload.listen();

  watch([path.watch.php], function () {
    gulp.start('php:build');
  });
  watch([path.watch.css], function () {
    gulp.start('css:build');
  });
  watch([path.watch.js], function () {
    gulp.start('js:build');
  });
  watch([path.watch.img], function () {
    gulp.start('img:build');
  });

});


gulp.task('default', ['build', 'watch']);