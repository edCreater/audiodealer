<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package  WooCommerce/Templates
 * @version     2.6.1
 */
global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop'] ++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}

?>

<div <?php post_class( 'products__item product' ); ?>>

	<div class="product__image">
		<a href="<?php the_permalink(); ?>">
			<?php echo get_the_post_thumbnail( $post->ID, 'shop_catalog' ) ?>
		</a>
	</div>
	<div class="product__meta">
		<div class="product__name">
			<a href="<?php the_permalink(); ?>">
				<span><?php the_title(); ?></span>
			</a>
		</div>

		<div class="product__excerpt">
			<?php woocommerce_template_single_excerpt(); ?>
		</div>

		<div class="product__price">
			<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
		</div>
	</div>
	<?php wc_get_template( 'loop/sale-flash.php' ); ?>

	<div class="product__hovers"><!-- start after shop loop item -->
		<a class="btn btn-default" href="<?php the_permalink(); ?>"><?php echo __( 'Узнать подробнее', 'audio' ); ?></a>
		<?php do_action( 'woocommerce_after_shop_loop_item' ); ?><!-- end after shop loop item -->
	</div>

</div>

