<?php
/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 22.10.2016
 */
?>

<?php $args = array(
		'post_type'      => 'cs_slider',
		'posts_per_page' => - 1
);
$slides     = new WP_Query( $args ); ?>
<?php if ( $slides->have_posts() ) : ?>
	<section class="h_slider swiper-container">
		<div class="swiper-wrapper">
			<?php while ( $slides->have_posts() ) : $slides->the_post(); ?>
				<?php $bg_image = get_post_meta( $post->ID, 'cs_slider_img', true ); ?>
				<div class="swiper-slide h_slider__item slide"
				     style="background-image: url('<?php echo ( isset( $bg_image['url'] ) ) ? $bg_image['url'] : ''; ?>')">
					<div class="container">
						<div class="row">
							<div class="col-sm-24 slide__thumb">
								<?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) ); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-8 slide__content">
								<div class="slide__text">
									<h2 div class="slide__title"><?php the_title(); ?></h2>
									<p><?php echo get_the_content(); ?></p>
								</div>
								<div class="slide__meta">
									<a class="btn btn-primary"
									   href="<?php the_permalink(); ?>"><?php echo __( 'Readmore', 'audio' ); ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
		<!-- Add Pagination -->
		<div class="h_slider__pagination"></div>
	</section>
<?php endif; ?>

