<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package CMSSuperHeroes
 * @subpackage CMS WP Puro
 * @since 1.0.0
 */
global $smof_data;
$allowed_html = cs_allowed_html();
?>
<!--Footer-->
<footer class="footer">
  <div class="footer__container container">
    <ul class="footer__nav">
      <li><a href="#">Бренды</a></li>
      <li><a href="#">Прокат</a></li>
      <li><a href="#">Сервисное оборудование</a></li>
      <li><a href="#">Проекты</a></li>
      <li><a href="#">О компании</a></li>
      <li><a href="#">Контакты</a></li>
    </ul>
    <div class="footer__social-links">
      <a class='social-link vk' href="#"></a>
      <a class='social-link insta' href="#"></a>
    </div>
    <div class="footer__phone"> <a href="#">+7 (4212) 644-622</a> </div>
    <hr>
    <div class="footer__copyrights"> Audiodealer.ru, 2016 </div>
    <div class="footer__payments"> <img src="<?php echo THEME_DIR_URI; ?>/assets/img/icons-payments.png" alt="payment systems"> </div>
  </div>
</footer>
<!--Footer.end-->
</div> <!-- End Frontent-wrpr -->
<?php wp_footer(); ?>
</body>

</html>