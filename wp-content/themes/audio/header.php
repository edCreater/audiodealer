<?php
/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 24.09.2016
 */

global $smof_data;
?>
<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!--[if lt IE 9]>
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/r29/html5.min.js"></script>
  <![endif]-->
  <title>Аудиодилер | Главная</title>
  <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
  <?php wp_head(); ?>
</head>

<body class="index-page desktop">
<!---->
<div id="common-wrpr">
  <!--Header-->
  <header>
    <a href="/" class="logo mobile-logo"></a>
    <div class="hamburger hamburger--arrow js-show-mobile-navigation">
      <div class="hamburger-box">
        <div class="hamburger-inner"></div>
      </div>
    </div>

    <div class="navigation">
      <div class="container lg-no-padding navigation__container">
        <nav class="navigation__nav col-md-24 col-lg-16 ">
          <?php wp_nav_menu(array(
              'theme_location' => 'main',
              'depth' => 2,
              'container' => '',
              'menu_class' => 'navigation__ul'
          )); ?>
          <form action="" class="navigation__search-form-sm">
            <input type="text" name="q">
            <button type="submit">!</button>
          </form>
        </nav>
        <div class="navigation__search col-lg-8 hidden-md-down visible-lg-block">
          <form action="" class="navigation__search-form">
            <input type="text" name="q">
            <button type="submit">!</button>
          </form>
        </div>
      </div>
    </div>

    <div class="container header">
      <div class="row header__row">
        <a href="/" class="header__logo">&nbsp;</a>
        <div class="header__cart">
          <div class="header__cart-products">Товаров: <span class="qty products-in-cart-qty">5</span></div>
          <div class="header__cart-summ">(<span class="summ-in-cart">1 235</span> руб.) </div>
        </div>

        <div class="header__lk">
          <div class="lk">Личный кабинет</div> <a href="#">Вход</a> / <a href="#">Регистрация</a>
        </div>

        <div class="header__phone"> <a href="#" class="phone no-decor">+7 (4212) 644-622</a>
          <div class="time">9:00 - 18:00</div>
        </div>
      </div>
    </div>

    <div class="navigation-categories">
      <div class="navigation-categories__container container">
        <ul class="navigation-categories__ul ">
          <li class="dropdown-label active" data-default-label='Категории'><a href="#">Категории</a></li>
          <?php wp_nav_menu(array(
              'theme_location' => 'categories',
              'depth' => 1,
              'container' => '',
              'items_wrap' => '%3$s'
          )); ?>
        </ul>
      </div>
    </div>

  </header><!--Header.end-->
  <!--Content-->
  <main>

    <?php if (is_front_page()) : ?>
    <!--Slider begin-->
      <?php get_template_part('templates/slider-home'); ?>
    <!--Slider end-->
    <?php endif; ?>
  </main><!--Main.end-->