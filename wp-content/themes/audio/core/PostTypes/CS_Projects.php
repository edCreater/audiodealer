<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 04.08.2015
 */
namespace SweetCore\PostTypes;

class CS_Projects
{
    function __construct(){

        $this->cs_projects_posttype();

    }

    function cs_projects_posttype() {

        register_post_type('cs_projects', array(
                'label'             => 'Проекты',
                'public'            => true,
                'show_ui'           => true,
                'show_in_nav_menus' => false,
                'hierarchical'      => false,
                'has_archive'       => false,
                'rewrite'           => array(
                		'slug' => 'projects'
                ),
                'supports'          => array(
                    'title', 'editor', 'thumbnail', 'excerpt'
                ),
            )
        );
    }

}