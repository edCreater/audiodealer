<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 04.08.2015
 */
namespace SweetCore\PostTypes;

class CS_Reviews
{
	function __construct(){

		$this->cs_reviews_posttype();

	}

	function cs_reviews_posttype() {

		register_post_type('cs_reviews', array(
				'label'             => 'Отзывы',
				'public'            => true,
				'show_ui'           => true,
				'show_in_nav_menus' => true,
				'rewrite'           => array(
					'slug'          => 'reviews',
				),
				'hierarchical'      => false,
				'has_archive'       => false,
				'supports'          => array(
					'title', 'post-formats', 'page-attributes', 'editor', 'thumbnail'
				),
			)
		);
	}

}