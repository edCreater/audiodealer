<?php
/**
 * Widget API: WP_Widget_Tags class
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 */

/**
 * Core class used to implement a Categories widget.
 *
 * @since 2.8.0
 *
 * @see WP_Widget
 */
class CS_Widget_Tax extends WP_Widget {

  /**
   * Sets up a new Tax widget instance.
   *
   * @since 2.8.0
   * @access public
   */
  public function __construct() {
    $widget_ops = array( 'classname' => 'cs_widget_tax', 'description' => __( "Tax term list." ) );
    parent::__construct('cs_tax', __('CS Tax'), $widget_ops);
  }

  /**
   * Outputs the content for the current Categories widget instance.
   *
   * @since 2.8.0
   * @access public
   *
   * @param array $args     Display arguments including 'before_title', 'after_title',
   *                        'before_widget', and 'after_widget'.
   * @param array $instance Settings for the current Categories widget instance.
   */
  public function widget( $args, $instance ) {
    static $first_dropdown = true;

    /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
    $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'CS Tax' ) : $instance['title'], $instance, $this->id_base );

    $taxonomies = ! empty( $instance['field_tax'] ) ? $instance['field_tax'] : '';
    $c = ! empty( $instance['count'] ) ? '1' : '0';
    $h = ! empty( $instance['hierarchical'] ) ? '1' : '0';

    echo $args['before_widget'];
    if ( $title ) {
      echo $args['before_title'] . $title . $args['after_title'];
    }

    $tax_args = array(
        'orderby'      => 'name',
        'show_count'   => $c,
        'hierarchical' => $h,
        'hide_empty' => true,
    );
    ?>

    <ul>
      <?
      $categories = get_terms($taxonomies, $tax_args);

      foreach ($categories as $cat) : ?>
        <li>
          <a href="<?= esc_url( get_category_link( $cat->term_id ) ) ?>">
            <? $catico = get_field('category-ico', $taxonomies . '_' . $cat->term_id); ?>
					<span class="ico" style="background: url(<?= $catico['url']; ?>) center center no-repeat">

					</span>
					<span class="name">
						<?= $cat->name; ?>
					</span>
          </a>
        </li>
      <? endforeach; ?>
    </ul>


    <?= $args['after_widget'];
  }

  /**
   * Handles updating settings for the current Categories widget instance.
   *
   * @since 2.8.0
   * @access public
   *
   * @param array $new_instance New settings for this instance as input by the user via
   *                            WP_Widget::form().
   * @param array $old_instance Old settings for this instance.
   * @return array Updated settings to save.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = sanitize_text_field( $new_instance['title'] );
    $instance['field_tax'] = sanitize_text_field( $new_instance['field_tax'] );
    $instance['count'] = !empty($new_instance['count']) ? 1 : 0;
    $instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;
    $instance['dropdown'] = !empty($new_instance['dropdown']) ? 1 : 0;

    return $instance;
  }

  /**
   * Outputs the settings form for the Categories widget.
   *
   * @since 2.8.0
   * @access public
   *
   * @param array $instance Current settings.
   */
  public function form( $instance ) {
    //Defaults
    $instance = wp_parse_args( (array) $instance, array( 'title' => '') );
    $title = sanitize_text_field( $instance['title'] );
    $field_tax = sanitize_text_field( $instance['field_tax'] );
    $count = isset($instance['count']) ? (bool) $instance['count'] :false;
    $hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
    $dropdown = isset( $instance['dropdown'] ) ? (bool) $instance['dropdown'] : false;
    ?>
    <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

    <p><label for="<?php echo $this->get_field_id('field_tax'); ?>"><?php _e( 'Tax:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('field_tax'); ?>" name="<?php echo $this->get_field_name('field_tax'); ?>" type="text" value="<?php echo esc_attr( $field_tax ); ?>" /></p>

    <p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('dropdown'); ?>" name="<?php echo $this->get_field_name('dropdown'); ?>"<?php checked( $dropdown ); ?> />
      <label for="<?php echo $this->get_field_id('dropdown'); ?>"><?php _e( 'Display as dropdown' ); ?></label><br />

      <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
      <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />

      <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"<?php checked( $hierarchical ); ?> />
      <label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e( 'Show hierarchy' ); ?></label></p>
    <?php
  }

}
