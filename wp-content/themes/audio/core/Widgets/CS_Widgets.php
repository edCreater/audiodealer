<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 06.08.2015
 */
namespace SweetCore\Widgets;

class CS_Widgets {

	function __construct() {
		$this->cs_widgets_init();
	}

	function cs_widgets_init() {
		if ( ! is_blog_installed() ) {
			return;
		}

		require_once( 'CS_Widget_Reviews.php' );
		register_widget( 'CS_Widget_Reviews' );

		require_once( 'CS_Widget_News.php' );
		register_widget( 'CS_Widget_News' );

		require_once( 'CS_Widget_Calc_Link.php' );
		register_widget( 'CS_Widget_Calc_Link' );

		do_action( 'widgets_init' );
	}

}



