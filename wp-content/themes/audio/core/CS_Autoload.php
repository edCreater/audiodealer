<?php
namespace SweetCore;

class CS_Autoload {
	public static $loader;

	public static function init() {
		if ( self::$loader == null ) {
			self::$loader = new self();
		}

		return self::$loader;
	}

	public function __construct() {
		spl_autoload_register(array('self', 'autoload'));
	}

	/*
	 * autoload class files from namespace uses
	 *
	 * @param    string    $className
	 */
	public static function autoload ($className) {
		$namespaces = explode('\\', $className);
		if (count($namespaces) > 1) {
			array_shift($namespaces);
			$classPath = dirname(__FILE__) . '/' . implode('/', $namespaces) . '.php';
			if (file_exists($classPath)) {
				require_once($classPath);
			}
		}
	}
}