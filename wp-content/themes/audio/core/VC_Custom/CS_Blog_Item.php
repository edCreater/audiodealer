<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Blog_Item
{

    public $params;

    function __construct(){
        $this->initParams();
        add_action('admin_init', array($this, 'blog_item_init'));
        add_shortcode('cs_blog_item', array($this, 'cs_blog_item_shortcode'));
    }

    function initParams(){

        $this->params = array(
            'name' => 'Blog Item',
            'description' => 'Blog item',
            'base' => 'cs_blog_item',
            'icon' => 'cs-blog-item',
            'category' => __( 'Content', 'cs'),
            'params' => array(
                array (
                    'param_name' 	=> 'blogitem',
                    'type' 			=> 'postlist',
                    'heading' 		=> __('Select Posts', 'mfn-opts'),
                    'admin_label'	=> true,
                    'query'			=> array(
                        'post_type'	=> 'post'
                    )
                ),

            )
        );
    }

    function blog_item_init(){
        if (function_exists('vc_map')){
            vc_map($this->params);
        }
    }

    function cs_blog_item_shortcode($atts, $content = null){
        $output = $blogitem = '';
        extract(shortcode_atts(array(
            'blogitem' => '',
        ), $atts));

        if ($blogitem) {
            $post_blog = get_post( $blogitem );
        }
        $subtitle = get_field('subtitle', $post_blog->ID, true);
        $time = get_field('time', $post_blog->ID, true);

        ob_start();
        ?>
        <div class="blog-item">
            <?php if (get_post_thumbnail_id($post_blog->ID)) : ?>
                <div class="thumb">
                    <div class="thumb-wrap">
                        <a href="<?php echo get_the_permalink($post_blog->ID); ?>">
                        <?php echo get_the_post_thumbnail($post_blog->ID, 'top-thumb', array('class'=>'img-responsive')); ?>
                        </a>
                        <div class="blog-meta">
                            <div class="author">
                                <div class="meta-ico">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="meta-content">
                                    <?php echo get_the_author_link($post_blog->ID); ?>
                                </div>
                            </div>
                            <div class="category">
                                <div class="meta-ico">
                                    <i class="fa fa-bars"></i>
                                </div>
                                <div class="meta-content">
                                    <?php echo get_the_category_list(' ','',$post_blog->ID); ?>
                                </div>
                            </div>
                            <div class="date">
                                <div class="meta-ico">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <div class="meta-content">
                                    <?php echo get_the_date('d.m', $post_blog->ID); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php $subtitle = get_field('subtitle', $post_blog->ID, true); ?>
            <div class="title">
                <h3><?php echo get_the_title($post_blog->ID); ?></h3>
                <p><?php echo $subtitle; ?></p>
            </div>
        </div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}