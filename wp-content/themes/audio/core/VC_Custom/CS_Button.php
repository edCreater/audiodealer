<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Button
{

    public $params;

    function __construct(){
        $this->initParams();
        add_action('admin_init', array($this, 'cs_button_init'));
        add_shortcode('cs_button', array($this, 'cs_button_shortcode'));
    }

    function initParams(){

        $this->params = array(
            'name' => 'Link Button',
            'description' => 'Button link',
            'base' => 'cs_button',
            'icon' => 'cs-button',
            'category' => __( 'Content', 'cs'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Title', 'cs'),
                    'value' => 'This is title',
                    'param_name' => 'title'
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Link', 'cs'),
                    'value' => 'This is link',
                    'param_name' => 'link'
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Custom class', 'cs'),
                    'value' => '',
                    'param_name' => 'custom_class'
                ),


            )
        );
    }

    function cs_button_init(){
        if (function_exists('vc_map')){
            vc_map($this->params);
        }
    }

    function cs_button_shortcode($atts, $content = null){
        $output = $link = $title = $custom_class = '';
        extract(shortcode_atts(array(
            'link' => '',
            'title' => '',
            'custom_class' => '',
            'content' => '',
        ), $atts));


        ob_start();
        ?>
        <a href="<?php echo $link; ?>" class="<?php echo $custom_class; ?>" ><?php echo $title; ?></a>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}