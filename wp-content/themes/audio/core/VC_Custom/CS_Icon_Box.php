<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Icon_Box
{

    public $params;

    function __construct(){
        $this->initParams();
        add_action('admin_init', array($this, 'icon_box_init'));
        add_shortcode('cs_icon_box', array($this, 'cs_icon_box_shortcode'));
    }

    function initParams(){

        $this->params = array(
            'name' => 'Icon Box',
            'description' => 'Icon box',
            'base' => 'cs_icon_box',
            'icon' => 'cs-icon-box',
            'category' => __( 'Content', 'cs'),
            'params' => array(
                array(
                    'type' => 'attach_image',
                    'heading' => __('Ico image', 'cs'),
                    'param_name' => 'icon_img'
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Title', 'cs'),
                    'value' => 'This is title',
                    'param_name' => 'title'
                ),
                array(
                    'type' => "textarea",
                    'heading' => __('Content', 'cs'),
                    'param_name' => 'icon_content',
                    'value' => ''
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Custom Style', 'cs'),
                    'value' => '',
                    'param_name' => 'custom_style'
                ),

            )
        );
    }

    function icon_box_init(){
        if (function_exists('vc_map')){
            vc_map($this->params);
        }
    }

    function cs_icon_box_shortcode($atts, $content = null){
        $output = $icon_img = $img_tag  = $title = $custom_style = $icon_content = '';
        extract(shortcode_atts(array(
            'icon_img' => '',
            'title' => '',
            'custom_style' => '',
            'icon_content' => '',
            'only_heading' => ''
        ), $atts));

        if ($icon_img) {
            $icon_img = wp_get_attachment_image_src($icon_img, 'large');
            $icon_img = $icon_img[0];
            $img_tag .= '<img src="' . $icon_img . '" alt="' . $title . '">';
        }

        ob_start();
        ?>
        <div class="cs-ibox <?php echo $custom_style; ?>">
            <div class="cs-ibox-heading">
                <div class="cs-ibox-thumb">
                    <?php echo $img_tag; ?>
                </div>
                <div class="cs-ibox-content">
                    <h3><?php echo $title; ?></h3>
                    <?php echo $icon_content; ?>
                </div>

            </div>
        </div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}