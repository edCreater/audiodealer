<?php

/**
 * Created by Ed.Creater <ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 03.04.2016
 */
namespace SweetCore\VC_Custom;

class CS_Develop
{
  public $params;

  function __construct(){
    $this->initParams();
    add_action('admin_init', array($this, 'develop_init'));
    add_shortcode('cs_develop', array($this, 'cs_develop_shortcode'));
  }

  function initParams(){

    $this->params = array(
        'name' => 'Develop',
        'description' => 'List Develops',
        'base' => 'cs_develop',
        'icon' => 'cs-develop',
        'category' => __( 'Content', 'cs'),
        'params' => array(
            array (
                'param_name' 	=> 'parent_page',
                'type' 			=> 'postlist',
                'heading' 		=> __('Select Parent Page', 'mfn-opts'),
                'admin_label'	=> true,
                'query'			=> array(
                    'post_type'	=> 'page'
                )
            ),

        )
    );
  }

  function develop_init(){
    if (function_exists('vc_map')){
      vc_map($this->params);
    }
  }

  function cs_develop_shortcode($atts, $content = null){
    $output = $parent_page = '';
    extract(shortcode_atts(array(
        'parent_page' => '',
    ), $atts));

    if ($parent_page) {

      ob_start();

      $args = array(
        'post_type' => 'page',
        'post_parent' => $parent_page
      );

      $develops = new \WP_Query($args);

      while ($develops->have_posts()) : $develops->the_post(); ?>
        <div class="develop-item row">
          <?php if (get_post_thumbnail_id($develops->ID)) : ?>
              <div class="col-sm-12">
                <div class="dev-thumb">
                  <a href="<?php echo the_permalink(); ?>">
                    <span class="thumb-wrap">
                        <?php echo the_post_thumbnail('top-thumb', array('class'=>'img-responsive')); ?>
                    </span>
                  </a>
                </div>
              </div>
            <div class="col-sm-12">
              <div class="dev-meta">
                <div class="author">
                  <div class="meta-ico">
                    <i class="fa fa-user"></i>
                  </div>
                  <div class="meta-content">
                    <?php echo get_the_author_link(); ?>
                  </div>
                </div>
                <div class="downloads">
                  <div class="meta-ico">
                    <i class="fa fa-download"></i>
                  </div>
                  <div class="meta-content">
                    Скачиваний: <?php echo 25; ?>
                  </div>
                </div>
                <div class="date">
                  <div class="meta-ico">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <div class="meta-content">
                    <?php echo get_the_date('d.m', get_the_ID()); ?>
                  </div>
                </div>
              </div>
              <div class="dev-title">
                <h2><? the_title(); ?></h2>
              </div>
              <div class="dev-content">
                <? the_excerpt(); ?>
              </div>
              <div class="dev-more">
                <a class="btn btn-red btn-lg" href="<? the_permalink(); ?>" title="<? the_title(); ?>"><i class="fa fa-eye"></i>Подробности</a>
              </div>
            </div>
          <?php endif; ?>
        </div>

      <? endwhile;
    }

    ?>
    <?php
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
  }


}