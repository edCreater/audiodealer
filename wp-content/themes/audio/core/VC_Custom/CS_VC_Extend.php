<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_VC_Extend
{
    public $icon_block;
    public $icon_box;
    public $portfolio_item;
    public $page_item;

    function __construct(){

        $this->cs_vc_types();

        $this->icon_block = new CS_Icon_Block();
        $this->icon_box = new CS_Icon_Box();
        $this->portfolio_item = new CS_Portfolio_Item();
        $this->blog_item = new CS_Blog_Item();
        $this->page_item = new CS_Page_Item();
        $this->develop = new CS_Develop();
        $this->posts_grid = new CS_Posts_Grid();
        $this->step = new CS_Step();
        $this->button = new CS_Button();
        $this->portfolio_masonry = new CS_Portfolio_Masonry();
        $this->popup = new CS_Popup_Form();

    }
    function cs_vc_types(){
        if ( class_exists('WPBakeryVisualComposerAbstract') ) {

            require 'types/postlist.php';

        }
    }

}