<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Portfolio_Masonry
{

  public $params;

  function __construct()
  {
    $this->initParams();
    add_action('admin_init', array($this, 'cs_portfolio_masonry_init'));
    add_shortcode('cs_portfolio_masonry', array($this, 'cs_portfolio_masonry_shortcode'));
  }

  function initParams()
  {

    $this->params = array(
        'name' => 'Portfolio Masonry',
        'description' => 'Portfolio masonry',
        'base' => 'cs_portfolio_masonry',
        'icon' => 'cs-portfolio-masonry',
        'category' => __('Content', 'cs'),
        'params' => array(
            array(
                'param_name' => 'portitem',
                'type' => 'postlist',
                'heading' => __('Select Testimonial', 'mfn-opts'),
                'admin_label' => true,
                'query' => array(
                    'post_type' => 'cs_portfolio'
                )
            ),

        )
    );
  }

  function cs_portfolio_masonry_init()
  {
    if (function_exists('vc_map')) {
      vc_map($this->params);
    }
  }

  function cs_portfolio_masonry_shortcode($atts, $content = null)
  {
    $output = $portitem = '';
    extract(shortcode_atts(array(
        'portitem' => '',
    ), $atts));

    $args = array(
        'post_type' => 'cs_portfolio'
    );
    $portfolio = new \WP_Query($args);

    ob_start();
    ?>
      <div class="port-masonry-outer">
    <div class="port-masonry-container">
      <div class="port-masonry-sizer"></div>
      <?php while ($portfolio->have_posts()) : $portfolio->the_post(); ?>
        <?php $width = get_post_meta(get_the_ID(), 'cs_port_masonry_width', true); ?>
        <?php $height = get_post_meta(get_the_ID(), 'cs_port_masonry_height', true); ?>

        <article class="port-masonry-item port-width-<?php echo $width; ?> port-height-<?php echo $height; ?>">
          <?php $subtitle = get_post_meta(get_the_ID(), 'subtitle', true); ?>
          <div class="port-masonry-inner">
            <div class="thumb">
            <div class="thumb-wrap">
              <?php the_post_thumbnail('full-thumb', array('class'=>'img-responsive')); ?>
            </div>
            </div>
            <div class="hover">
            </div>
            <div class="hovered">
              <a href="<?php the_permalink(); ?>">
                <div class="plus">
                  <a href="http://codesweet.dev/cs_portfolio/shablon-dlya-vordpress/">
                    <i class="fa fa-gg"></i>
                  </a>
                </div>
              </a>
            </div>
            <div class="title"><span class="port-ico"><i class="fa fa-desktop"></i></span><span class="port-text"><?php the_title(); ?></span></div>
          </div>
        </article>
      <?php endwhile; ?>
    </div>
      </div>
    <?php
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
  }
}