<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
namespace SweetCore\VC_Custom;

class CS_Icon_Block
{

    public $params;

    function __construct(){
        $this->initParams();
        add_action('admin_init', array($this, 'icon_block_init'));
        add_shortcode('cs_icon_block', array($this, 'cs_icon_block_shortcode'));
    }

    function initParams(){

        $this->params = array(
            'name' => 'Icon Block',
            'description' => 'Icon block',
            'base' => 'cs_icon_block',
            'icon' => 'cs-icon-block',
            'category' => __( 'Content', 'cs'),
            'params' => array(
                array(
                    'type' => 'attach_image',
                    'heading' => __('Ico image', 'cs'),
                    'param_name' => 'icon_img'
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Title', 'cs'),
                    'value' => 'This is title',
                    'param_name' => 'title'
                ),
                array(
                    'type' => "textarea",
                    'heading' => __('Content', 'cs'),
                    'param_name' => 'icon_content',
                    'value' => ''
                ),

                array(
                    "type" => "checkbox",
                    "heading" => __( "Only Heading"),
                    "param_name" => "only_heading",
                    "description" => __( "Only heading style", "cs" ),
                    'admin_label'	=> true,
                ),
                array (
                    'param_name' 	=> 'extraclassname',
                    'type' 			=> 'textfield',
                    'heading' 		=> __('Extra Class Name', 'cs'),
                    'admin_label'	=> true,
                    'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',
                ),
            )
        );
    }

    function icon_block_init(){
        if (function_exists('vc_map')){
            vc_map($this->params);
        }
    }

    function cs_icon_block_shortcode($atts, $content = null){
        $output = $only_heading = $icon_img = $img_tag  = $title = $icon_content = $extraclassname = '';
        extract(shortcode_atts(array(
            'icon_img' => '',
            'title' => '',
            'icon_content' => '',
            'only_heading' => '',
            'extraclassname' => ''
        ), $atts));

        if ($icon_img) {
            $icon_img = wp_get_attachment_image_src($icon_img, 'large');
            $icon_img = $icon_img[0];
            $img_tag .= '<img src="' . $icon_img . '" alt="' . $title . '">';
        }

        ob_start();
        ?>
        <div class="cs-icon-block <?php echo ($only_heading) ? 'cs-icon-only-heading' : ''; ?>">
            <div class="cs-icon-heading">
                <div class="cs-icon-thumb">
                    <?php echo $img_tag; ?>
                </div>
                <div class="cs-icon-title <?php echo $extraclassname; ?>">
                    <h3><?php echo $title; ?></h3>
                </div>

            </div>
            <div class="cs-icon-content">
                <?php echo $icon_content; ?>
            </div>
        </div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}