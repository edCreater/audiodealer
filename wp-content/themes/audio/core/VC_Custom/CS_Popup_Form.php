<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 16.08.2015
 */
 namespace SweetCore\VC_Custom;
 
class CS_Popup_Form
{

    public $params;

    function __construct(){
        $this->initParams();
        add_action('admin_init', array($this, 'cs_popup_form_init'));
        add_shortcode('cs_popup_form', array($this, 'cs_popup_form_shortcode'));
    }

    function initParams(){

        $this->params = array(
            'name' => 'Popup Form',
            'description' => 'Button with popup form',
            'base' => 'cs_popup_form',
            'icon' => 'cs-popup-form',
            'category' => __( 'Content', 'cs'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Title', 'cs'),
                    'value' => 'This is title',
                    'param_name' => 'title'
                ),
                array(
                    "type" => "textarea_html",
                    "heading" => "Form text",
                    "param_name" => "content",
                    "value" => ""
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Btn title', 'cs'),
                    'value' => 'Button title',
                    'param_name' => 'btn_title'
                ),
                array(
                    "type" => "dropdown",
                    'holder' => 'div',
                    'heading' => __('Type button', 'cs'),
                    'value' => '',
                    'param_name' => 'btn_type',
                    "value" => array(
                        "Style 1 - Default Style" => "style1",
                        "Style 2 - Trnsparent" => "style2"
                    )
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'heading' => __('Custom class', 'cs'),
                    'value' => '',
                    'param_name' => 'custom_class'
                ),


            )
        );
    }

    function cs_popup_form_init(){
        if (function_exists('vc_map')){
            vc_map($this->params);
        }
    }

    function cs_popup_form_shortcode($atts, $content = null){
        $output = $btn_type = $btn_title = $title = $custom_class = '';
        extract(shortcode_atts(array(
            'title' => '',
            'custom_class' => '',
            'btn_title' => '',
            'btn_type' => ''
        ), $atts));


        ob_start();

      ?>
        <div class="pform-block <?= $custom_class; ?>">
          <p class="pform-title"><?= $title; ?></p>
        <?
        switch ($btn_type) {
            case 'style1':
                $btn = '<a href="#service-order" class="btn btn-primary fancybox" >' . $btn_title . '</a>';
                break;
            case 'style2':
              $btn = '<a href="#service-order" class="btn btn-white-trans fancybox">' . $btn_title . '</a>';
              break;
            default:
                $btn = '<a href="#service-order" class="btn btn-primary fancybox" >' . $btn_title . '</a>';
                break;
        }

        echo $btn;

        ?>
        <div class="pform-modal">
            <div id="service-order" class="popup-form">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>

        </div>

      <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}