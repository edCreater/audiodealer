<?php
/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 24.09.2016
 */

function cs_allowed_html(){
  $allowed_html = array(
      'a' => array(
          'href' => array(),
          'title' => array()
      ),
      'img' => array(
          'src' => array(),
          'title' => array(),
          'alt' => array()
      ),
      'br' => array(),
      'em' => array(),
      'strong' => array(),
      'p' => array(),
      'span' => array(
          'class' => array()
      )
  );
  return $allowed_html;
}