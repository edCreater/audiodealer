<?php
/*
Plugin Name: Demo MetaBox
Plugin URI: http://en.bainternet.info
Description: My Meta Box Class usage demo
Version: 3.1.1
Author: Bainternet, Ohad Raz
Author URI: http://en.bainternet.info
*/

if (is_admin()){
  /* 
   * prefix of meta keys, optional
   * use underscore (_) at the beginning to make keys hidden, for example $prefix = '_ba_';
   *  you also can make prefix empty to disable it
   * 
   */
  $prefix = 'cs_';




  /*
   * 2nd metabox
   */
/*
  $config = array(
      'id'             => 'post_meta_box',          // meta box id, unique per meta box
      'title'          => 'Page options',          // meta box title
      'pages'          => array('post', 'page'),      // post types, accept custom post types as well, default is array('post'); optional
      'context'        => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
      'priority'       => 'high',            // order of meta box: high (default), low; optional
      'fields'         => array(),            // list of meta fields (can be added by field arrays)
      'local_images'   => false,          // Use local or hosted images (meta box images for add/remove)
      'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );

  $my_meta2 =  new AT_Meta_Box($config);

  $my_meta2->addImage($prefix.'head_image',array('name'=> 'Head Image '));

  $my_meta2->Finish();

  */
  /*
   * Subtitle metabox
   */

  $config = array(
      'id'             => 'options_meta_box',          // meta box id, unique per meta box
      'title'          => 'Post options',          // meta box title
      'pages'          => array('post', 'page', 'cs_portfolio'),      // post types, accept custom post types as well, default is array('post'); optional
      'context'        => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
      'priority'       => 'high',            // order of meta box: high (default), low; optional
      'fields'         => array(),            // list of meta fields (can be added by field arrays)
      'local_images'   => false,          // Use local or hosted images (meta box images for add/remove)
      'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );

  $event_meta =  new AT_Meta_Box($config);

  $event_meta->addText($prefix . 'subtitle',array('name'=> 'Subtitle '));

  $event_meta->Finish();


  /*
   * Page metabox
   */

  $config = array(
      'id'             => 'page_meta_box',          // meta box id, unique per meta box
      'title'          => 'Page options',          // meta box title
      'pages'          => array('page'),      // post types, accept custom post types as well, default is array('post'); optional
      'context'        => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
      'priority'       => 'high',            // order of meta box: high (default), low; optional
      'fields'         => array(),            // list of meta fields (can be added by field arrays)
      'local_images'   => false,          // Use local or hosted images (meta box images for add/remove)
      'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );

  $page_meta =  new AT_Meta_Box($config);

  $page_meta->addImage($prefix.'mtranse_head_img',array('name'=> 'Heading image '));

  $page_meta->Finish();



  $config = array(
    'id'             => 'gallery',          // meta box id, unique per meta box
    'title'          => 'Gallery',          // meta box title
    'pages'          => array('cs_projects'),      // post types, accept custom post types as well, default is array('post'); optional
    'context'        => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'priority'       => 'high',            // order of meta box: high (default), low; optional
    'fields'         => array(),            // list of meta fields (can be added by field arrays)
    'local_images'   => false,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );

  $project_gallery =  new AT_Meta_Box($config);

  $repeater_fields[] = $project_gallery->addImage($prefix.'projects_gallery_img',array('name'=> 'Image '), true);

	$project_gallery->addRepeaterBlock($prefix.'project_gallery',array(
    'inline'   => true,
    'name'     => 'Gallery images',
    'fields'   => $repeater_fields,
    'sortable' => true
  ));

	$project_gallery->Finish();

  /*
 * Calculator metabox Options
 */

  $config = array(
      'id'             => 'calc_options_meta_box',          // meta box id, unique per meta box
      'title'          => 'Calculator options',          // meta box title
      'pages'          => array('cs_calc'),      // post types, accept custom post types as well, default is array('post'); optional
      'context'        => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
      'priority'       => 'high',            // order of meta box: high (default), low; optional
      'fields'         => array(),            // list of meta fields (can be added by field arrays)
      'local_images'   => false,          // Use local or hosted images (meta box images for add/remove)
      'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );

  $calc_options_meta =  new AT_Meta_Box($config);

  $calc_options_meta->addText($prefix . 'style',array('name'=> 'CSS Класс(служебное) '));

  $calc_options_meta->Finish();

  /*
	 * Slider Metabox
	 */

  $config = array(
      'id'             => 'slider_meta',          // meta box id, unique per meta box
      'title'          => 'Slider metabox',          // meta box title
      'pages'          => array('cs_slider'),      // post types, accept custom post types as well, default is array('post'); optional
      'context'        => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
      'priority'       => 'high',            // order of meta box: high (default), low; optional
      'fields'         => array(),            // list of meta fields (can be added by field arrays)
      'local_images'   => false,          // Use local or hosted images (meta box images for add/remove)
      'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );

  $slider_meta =  new AT_Meta_Box($config);

	$slider_meta->addImage($prefix . 'slider_img', array('name'=> 'Image background '));
	$slider_meta->addText($prefix . 'slider_link',array('name'=> 'Ссылка '));

	$slider_meta->Finish();


	/**
	 * Taxonomy Metaboxes
	 */

	$prefix = 'cs_';

	$config = array(
			'id' => 'barnd_box',          // meta box id, unique per meta box
			'title' => 'Brands settings',          // meta box title
			'pages' => array('product_brand'),        // taxonomy name, accept categories, post_tag and custom taxonomies
			'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
			'fields' => array(),            // list of meta fields (can be added by field arrays)
			'local_images' => false,          // Use local or hosted images (meta box images for add/remove)
			'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
	);
	/*
	* Initiate your meta box
	*/
	$brand_meta =  new Tax_Meta_Class($config);

	$brand_meta->addImage($prefix.'image_brand_thumb',array('name'=> __('Brand Logo image ','cs')));

	$brand_meta->Finish();
}
