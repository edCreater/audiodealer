/**
 * Created by EdCreater.
 * Email: ed.creater@gmail.com
 * Site: codesweet.ru
 * Date: 06.10.2015
 */
$(window).load(function(){
    $('.equalheight>div').equalHeights();
})
$(window).resize(function(){
    $('.equalheight>div').equalHeights();
})
