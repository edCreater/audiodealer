<?php
/**
 * Created by EdCreater.
 * Email: ed.creater@gmail.com
 * Site: codesweet.ru
 * Date: 06.10.2015
 */
namespace SweetCore\Helpers;

class CS_Menu_Walker extends \Walker_Nav_Menu {
    function start_el(&$output, $item,  $depth = 0, $args = array(), $current_object_id = 0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
        if ($item->menu_item_parent == 0 ) {
            $attributes .= ' itemprop="url"';
        }
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

        $item_output = $args->before;
        if ($item->menu_item_parent == 0 ) {
            $item_output .= '<a'. $attributes .'><p class="strong" itemprop="name">';
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            //if (strripos($class_names,'menu-item-has-children')) {
            $item_output .= '</p><p class="sub" itemprop="description">' . $item->description . '</p>';
            //}
        } else{
            $item_output .= '<a'. $attributes .'><p class="strong">';
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            //if (strripos($class_names,'menu-item-has-children')) {
            $item_output .= '</p><p class="sub">' . $item->description . '</p>';
            //}
        }
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}