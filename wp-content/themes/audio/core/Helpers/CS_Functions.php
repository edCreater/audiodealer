<?php

/**
 * Created by EdCreater.
 * Site: codesweet.ru
 * Email: ed.creater@gmail.com
 * Date: 22.08.2015
 */
namespace SweetCore\Helpers;

class CS_Functions {

	public static function cs_excerpt( $args = '' ) {
		global $post;

		$default = array( 'postid'      => '',
		                  'maxchar'     => 350,
		                  'text'        => '',
		                  'save_format' => false,
		                  'more_text'   => 'Читать дальше...',
		                  'echo'        => true,
		);

		parse_str( $args, $_args );
		$args = array_merge( $default, $_args );
		extract( $args );
		if ( $postid ) {
			$post = get_post( $postid );
		}

		if ( ! $text ) {
			$text = $post->post_excerpt ? $post->post_excerpt : $post->post_content;

			$text = preg_replace( '~\[[^\]]+\]~', '', $text ); // убираем шоткоды, например:[singlepic id=3]
			// $text = strip_shortcodes( $text ); // или можно так обрезать шоткоды, так будет вырезан шоткод и конструкция текста внутри него
			// и только те шоткоды которые зарегистрированы в WordPress. И эта операция быстрая, но она в десятки раз дольше предыдущей
			// (хотя там очень маленькие цифры в пределах одной секунды на 50000 повторений)

			// для тега <!--more-->
			if ( ! $post->post_excerpt && strpos( $post->post_content, '<!--more-->' ) ) {
				preg_match( '~(.*)<!--more-->~s', $text, $match );
				$text = trim( $match[1] );
				$text = str_replace( "\r", '', $text );
				$text = preg_replace( "~\n\n+~s", "</p><p>", $text );
				$text = '<p>' . str_replace( "\n", '<br />', $text ) . ' <a href="' . get_permalink( $post->ID ) . '#more-' . $post->ID . '">' . $more_text . '</a></p>';

				if ( $echo ) {
					return print $text;
				}

				return $text;
			} elseif ( ! $post->post_excerpt ) {
				$text = strip_tags( $text, $save_format );
			}
		}

		// Обрезаем
		if ( mb_strlen( $text ) > $maxchar ) {
			mb_internal_encoding( 'UTF-8' );
			$text = mb_substr( $text, 0, $maxchar );
			$text = preg_replace( '@(.*)\s[^\s]*$@s', '\\1 ...', $text ); // убираем последнее слово, оно 99% неполное
		}

		// Сохраняем переносы строк. Упрощенный аналог wpautop()
		if ( $save_format ) {
			$text = str_replace( "\r", '', $text );
			$text = preg_replace( "~\n\n+~", "</p><p>", $text );
			$text = "<p>" . str_replace( "\n", "<br />", trim( $text ) ) . "</p>";
		}

		//$out = preg_replace('@\*[a-z0-9-_]{0,15}\*@', '', $out); // удалить *some_name-1* - фильтр сммайлов

		if ( $echo ) {
			return print $text;
		}

		return $text;
	}




	/*
* "Хлебные крошки" для WordPress
* автор: Dimox
* версия: 2015.05.21
*/
	public static function cs_breadcrumbs() {

		/* === ОПЦИИ === */
		$basics = get_option('cs_basics');

		$text['home']     = 'Главная'; // текст ссылки "Главная"
		$text['category'] = '%s'; // текст для страницы рубрики
		$text['search']   = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
		$text['tag']      = 'Записи с тегом "%s"'; // текст для страницы тега
		$text['author']   = 'Статьи автора %s'; // текст для страницы автора
		$text['404']      = 'Ошибка 404'; // текст для страницы 404
		$text['page']     = 'Страница %s'; // текст 'Страница N'
		$text['cpage']    = 'Страница комментариев %s'; // текст 'Страница комментариев N'

		$delimiter      = '/'; // разделитель между "крошками"
		$delim_before   = '<span class="divider">'; // тег перед разделителем
		$delim_after    = '</span>'; // тег после разделителя
		$show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
		$show_on_home   = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
		$show_title     = 1; // 1 - показывать подсказку (title) для ссылок, 0 - не показывать
		$show_current   = 1; // 1 - показывать название текущей страницы, 0 - не показывать
		$before         = '<span class="current">'; // тег перед текущей "крошкой"
		$after          = '</span>'; // тег после текущей "крошки"
		/* === КОНЕЦ ОПЦИЙ === */

		global $post;
		$home_link      = home_url( '/' );
		$link_before    = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
		$link_after     = '</span>';
		$link_attr      = ' itemprop="url"';
		$link_in_before = '<span itemprop="title">';
		$link_in_after  = '</span>';
		$link           = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
		$frontpage_id   = get_option( 'page_on_front' );
		if ( $post ) {
			$parent_id = $post->post_parent;
		}
		$delimiter = ' ' . $delim_before . $delimiter . $delim_after . ' ';

		if ( is_home() || is_front_page() ) {

			if ( $show_on_home == 1 ) {
				echo '<div class="cs-breadcrumbs"><a href="' . $home_link . '">' . $text['home'] . '</a></div>';
			}

		} else {

			echo '<div class="cs-breadcrumbs">';
			if ( $show_home_link == 1 ) {
				echo sprintf( $link, $home_link, $text['home'] );
			}

			if ( is_category() ) {
				$cat = get_category( get_query_var( 'cat' ), false );
				if ( $cat->parent != 0 ) {
					$cats = get_category_parents( $cat->parent, true, $delimiter );
					$cats = preg_replace( "#^(.+)$delimiter$#", "$1", $cats );
					$cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats );
					if ( $show_title == 0 ) {
						$cats = preg_replace( '/ title="(.*?)"/', '', $cats );
					}
					if ( $show_home_link == 1 ) {
						echo $delimiter;
					}
					echo $cats;
				}
				if ( get_query_var( 'paged' ) ) {
					$cat         = $cat->cat_ID;
					$small_title = get_field( 'small_title', 'category_' . $cat );
					if ( $small_title ) {
						$title = $small_title;
					} else {
						$title = get_cat_name( $cat );
					}
					echo $delimiter . sprintf( $link, get_category_link( $cat ), $title ) . $delimiter . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
				} else {

					$title = single_cat_title( '', false );

					if ( $show_current == 1 ) {
						echo $delimiter . $before . sprintf( $text['category'], $title ) . $after;
					}
				}

			} elseif ( is_search() ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				echo $before . sprintf( $text['search'], get_search_query() ) . $after;

			} elseif ( is_day() ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
				echo sprintf( $link, get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), get_the_time( 'F' ) ) . $delimiter;
				echo $before . get_the_time( 'd' ) . $after;

			} elseif ( is_month() ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
				echo $before . get_the_time( 'F' ) . $after;

			} elseif ( is_year() ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				echo $before . get_the_time( 'Y' ) . $after;

			} elseif ( is_single() && ! is_attachment() ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				if (( get_post_type() != 'post' ) ) {
					$post_type = get_post_type_object( get_post_type() );
					$slug      = $post_type->rewrite;
					printf( $link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name );
					if ( $show_current == 1 ) {
						echo $delimiter . $before . get_the_title() . $after;
					}
				} elseif ( get_post_type() == 'cs_services' ) {
					$post_type = get_post_type_object( get_post_type() );
					$post_type_page = $basics['cs_services_page'];
					$link_post_type = get_page_link($post_type_page);

					printf( $link, $link_post_type, $post_type->label );
					if ( $show_current == 1 ) {
						echo $delimiter . $before . get_the_title() . $after;
					}
				} else {
					$title = get_the_title();

					$cat  = get_the_category();
					$cat  = $cat[0];
					$cats = self::cs_get_category_parents( $cat, true, $delimiter );
					if ( $show_current == 0 || get_query_var( 'cpage' ) ) {
						$cats = preg_replace( "#^(.+)$delimiter$#", "$1", $cats );
					}
					$cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats );
					if ( $show_title == 0 ) {
						$cats = preg_replace( '/ title="(.*?)"/', '', $cats );
					}
					echo $cats;
					if ( get_query_var( 'cpage' ) ) {
						echo $delimiter . sprintf( $link, get_permalink(), $title ) . $delimiter . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
					} else {
						if ( $show_current == 1 ) {
							echo $before . $title . $after;
						}
					}
				}

				// custom post type
			} elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' && ! is_404() && ! is_tax() ) {
				$post_type = get_post_type_object(get_post_type());
				if (get_query_var('paged')) {
					echo $delimiter . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $delimiter . $before . sprintf($text['page'], get_query_var('paged')) . $after;
				} else {
					if ($show_current == 1) {
						echo $delimiter . $before . $post_type->label . $after;
					}
				}
				// custom tax
			} elseif(is_tax()){

				$term_slug = get_query_var( 'term' );
				$tax_name = get_query_var( 'taxonomy' );
				$term = get_term_by('slug', $term_slug, $tax_name);
				if ( $term->parent != 0 ) {

					$terms = CS_Functions::get_taxterm_parents( $tax_name, $term->parent, true, $delimiter );
					$terms = preg_replace( "#^(.+)$delimiter$#", "$1", $terms );
					$terms = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $terms );
					if ( $show_title == 0 ) {
						$cats = preg_replace( '/ title="(.*?)"/', '', $terms );
					}
					if ( $show_home_link == 1 ) {
						echo $delimiter;
					}
					echo $terms;
				}
				if ( get_query_var( 'paged' ) ) {
					$title = $term->name;
					echo $delimiter . sprintf( $link, get_category_link( $term ), $title ) . $delimiter . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
				} else {

					$title = single_term_title( '', false );
					if ( $show_current == 1 ) {
						echo $delimiter . $before . sprintf( $text['category'], $title ) . $after;
					}
				}

			} elseif ( is_attachment() ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				$parent = get_post( $parent_id );
				$cat    = get_the_category( $parent->ID );
				$cat    = $cat[0];
				if ( $cat ) {
					$cats = get_category_parents( $cat, true, $delimiter );
					$cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats );
					if ( $show_title == 0 ) {
						$cats = preg_replace( '/ title="(.*?)"/', '', $cats );
					}
					echo $cats;
				}
				printf( $link, get_permalink( $parent ), $parent->post_title );
				if ( $show_current == 1 ) {
					echo $delimiter . $before . get_the_title() . $after;
				}

			} elseif ( is_page() && ! $parent_id ) {
				if ( $show_current == 1 ) {
					echo $delimiter . $before . get_the_title() . $after;
				}

			} elseif ( is_page() && $parent_id ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				if ( $parent_id != $frontpage_id ) {
					$breadcrumbs = array();
					while ( $parent_id ) {
						$page = get_page( $parent_id );
						if ( $parent_id != $frontpage_id ) {
							$breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
						}
						$parent_id = $page->post_parent;
					}
					$breadcrumbs = array_reverse( $breadcrumbs );
					for ( $i = 0; $i < count( $breadcrumbs ); $i ++ ) {
						echo $breadcrumbs[ $i ];
						if ( $i != count( $breadcrumbs ) - 1 ) {
							echo $delimiter;
						}
					}
				}
				if ( $show_current == 1 ) {
					echo $delimiter . $before . get_the_title() . $after;
				}

			} elseif ( is_tag() ) {
				if ( $show_current == 1 ) {
					echo $delimiter . $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
				}

			} elseif ( is_author() ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				global $author;
				$author = get_userdata( $author );
				echo $before . sprintf( $text['author'], $author->display_name ) . $after;

			} elseif ( is_404() ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				echo $before . $text['404'] . $after;

			} elseif ( has_post_format() && ! is_singular() ) {
				if ( $show_home_link == 1 ) {
					echo $delimiter;
				}
				echo get_post_format_string( get_post_format() );
			}

			echo '</div><!-- .breadcrumbs -->';

		}
	} // end dimox_breadcrumbs()

	public static function  cs_get_category_parents( $id, $link = false, $separator = '/', $nicename = false, $visited = array() ) {
		$chain = '';
		$parent = get_term( $id, 'category' );
		if ( is_wp_error( $parent ) )
			return $parent;

		if ( $nicename )
			$name = $parent->slug;
		else
			$name = $parent->name;

		if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
			$visited[] = $parent->parent;
			$chain .= get_category_parents( $parent->parent, $link, $separator, $nicename, $visited );
		}

		if ( $link )
			$chain .= '<a href="' . esc_url( get_category_link( $parent->term_id ) ) . '">'.$name.'</a>' . $separator;
		else
			$chain .= $name.$separator;
		return $chain;
	}

	public static function cs_extend_cf7( $tag, $unused ) {

		if ( $tag['name'] != 'regions-list' ) {
			return $tag;
		}

		$args    = array(
			'post_type'   => 'cs_regions',
			'numberposts' => 50,
			'orderby'     => 'title',
			'order'       => 'ASC'
		);
		$plugins = get_posts( $args );

		if ( ! $plugins ) {
			return $tag;
		}

		foreach ( $plugins as $plugin ) {
			$tag['raw_values'][] = $plugin->post_title;
			$tag['values'][]     = $plugin->post_title;
			$tag['labels'][]     = $plugin->post_title;
		}

		return $tag;
	}

	public static function get_children_pages() {
		global $post;

		$children = wp_list_pages( "title_li=&child_of=" . $post->ID . "&depth=1&echo=0" );
		if ( $children ) { ?>
			<div class="childrens-container">
				<ul class="links-menu"><?php echo $children; ?></ul>
			</div>
		<?php }
	}
	public static function get_taxterm_parents( $taxonomy, $id, $link = false, $separator = '/', $nicename = false, $visited = array() ) {
		$chain = '';
		$parent = get_term( $id, $taxonomy );
		if ( is_wp_error( $parent ) )
			return $parent;

		if ( $nicename )
			$name = $parent->slug;
		else
			$name = $parent->name;

		if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
			$visited[] = $parent->parent;
			$chain .= get_taxterm_parents( $parent->parent, $link, $separator, $nicename, $visited );
		}

		if ( $link )
			$chain .= '<a href="' . esc_url( get_term_link( $parent->term_id, $taxonomy ) ) . '">'.$name.'</a>' . $separator;
		else
			$chain .= $name.$separator;
		return $chain;
	}
	public static function get_first_paren_term( $taxonomy, $id, $nicename = false, $visited = array() ) {
		$chain = '';
		$parent = get_term( $id, $taxonomy );
		if ( is_wp_error( $parent ) )
			return $parent;

		$name = $parent->slug;

		if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
			$visited[] = $parent->parent;
			$chain = get_first_paren_term( $parent->parent, $nicename, $visited );
		}

		$chain .= $name;

		return $chain;
	}

	/* Функция вывода постов по месяцу.
 ----------
 * Параметры:
 * $show_comment_count(0) - показывать ли колличество комментариев. 1 - показывать.
 * $before ('<h4>') - HTML тег до заголовка (названия месяца).
 * $after ('</h4>') - HTML тег после заголовка.
 * $year (0) - огриничить вывод годом. Если указать 2009, будут выведены записи за 2009 год по месяцам
 * $post_type ('post') - тип поста. Если нужно вывести нестандартный тип постов (отличный от post)
 * $limit(100) - ограничение количества выводиммых постов для каждого месяца. При большой базе, создается сильная нагрузка. Укажите 0 если нужно снять ограничение
 ----------
 Пример вызова: <php echo get_blix_archive(1, '<h4>', '</h4>'); ?>
*/
	public static function cs_archive_per_month($cat=''){
		global $flag_month, $wpdb;
		$result = '';

		$arcresults = $wpdb->get_results("SELECT *
			FROM wp_posts AS p
			INNER JOIN wp_term_relationships AS tr ON p.ID = tr.object_id
			INNER JOIN wp_term_taxonomy AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
			INNER JOIN wp_terms AS t ON tt.term_id = t.term_id
			WHERE t.term_id = $cat
			ORDER BY post_date DESC");

		$m_num = Array('01', '02', '03', '04', '05', '06','07', '08', '09', '10', '11', '12');
		$m_names = Array('Январь ', 'Февраль ','Март ', 'Апрель ','Май ', 'Июнь ','Июль ', 'Август ','Сентябрь ', 'Октябрь ','Ноябрь','Декабрь ');

		foreach ($arcresults as $arcresult) {

			$post_date = date("d/m/y",strtotime($arcresult->post_date));
			if ($flag_month <> substr($post_date,3,2)){

				$flag_month = substr($post_date,3,2);
				?>
				<h2 class="month">
					<?php echo str_replace($m_num,$m_names,$flag_month); ?>
				</h2>
			<?php } ?>

			<div class="post-prev row">
				<div class="col-sm-3">
					<span class="cat-date"><?php echo $post_date; ?></span>
				</div>
				<div class="col-sm-21">
					<h3 class="post-title"><a href="<?php echo get_the_permalink($arcresult->ID) ?>"><?php echo $arcresult->post_title; ?></a></h3>
				</div>
			</div>
			<?php

		}
		return true;
	}

	// function Russian Date
	public static function the_russian_time($template='') {

		$RinTemplate = strpos($template, "R");

		if ($RinTemplate===FALSE) {
			echo get_the_time($template);
		} else {
			if($RinTemplate > 0) {
				echo get_the_time(substr($template, 0,$RinTemplate));
			}

			$months= array (
				"января",
				"февраля",
				"марта",
				"апреля",
				"мая",
				"июня",
				"июля",
				"августа",
				"сентября",
				"октября",
				"ноября",
				"декабря"
			);
			echo $months[get_the_time('n')-1];
			self::the_russian_time(substr($template,$RinTemplate+1));
		}
	}

	public static function get_rus_date($timestamp=NULL) {

		if (!$timestamp){
			return false;
		}
		$months= array (
				"января",
				"февраля",
				"марта",
				"апреля",
				"мая",
				"июня",
				"июля",
				"августа",
				"сентября",
				"октября",
				"ноября",
				"декабря"
		);

		$date=explode(".", date("d.m.Y", $timestamp));

		return $date[0].'&nbsp;'.$months[$date[1]-1].'&nbsp;'.$date[2];


	}


}