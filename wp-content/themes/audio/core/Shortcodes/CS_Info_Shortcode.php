<?php

/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 01.08.2016
 */
namespace SweetCore\Shortcodes;

class CS_Info_Shortcode {

	public $params;

	function __construct() {

		add_shortcode( 'cs_info', array( $this, 'cs_info_shortcode' ) );

	}

	function cs_info_shortcode( $atts, $content = null ) {

		ob_start();
		?>
		<div class="cs-info"><?php echo $content; ?></div>
		<?
		$output_string=ob_get_contents();
		ob_end_clean();

		return $output_string;
	}


}