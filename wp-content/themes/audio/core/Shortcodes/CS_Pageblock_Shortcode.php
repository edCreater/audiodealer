<?php

/**
 * Created by Ed.Creater<ed.creater@gmail.com>.
 * Site: CodeSweet.ru
 * Date: 01.08.2016
 */
namespace SweetCore\Shortcodes;

class CS_Pageblock_Shortcode {

  public $params;

  function __construct() {

    add_shortcode( 'cs_pageblock', array( $this, 'cs_pageblock_shortcode' ) );

  }

  function cs_pageblock_shortcode( $atts, $content = null ) {

    ob_start();
    ?>
    <div class="cs-info"><?php echo $content; ?></div>
    <?
    $output_string=ob_get_contents();
    ob_end_clean();

    return $output_string;
  }


}