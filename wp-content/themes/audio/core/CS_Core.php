<?php

/**
 * Created by EdCreater.
 * Email: ed.creater@gmail.com
 * Site: codesweet.ru
 * Date: 02.10.2015
 */
namespace SweetCore;


class CS_Core {

	public $theme_settings;

	function __construct() {

    require_once "CS_Autoload.php";
    $autoloader = new CS_Autoload();

    // Require Other Libraries
    $libs = new Libs\CS_Libs();

    require_once SWEETCORE_ADMIN_DIR . '/options/functions.php';
    require_once SWEETCORE_ADMIN_DIR . '/plugins/setup-plugins.php';
    require_once SWEETCORE_ADMIN_DIR . '/metaboxes/metaboxes.init.php';


    // Init CS Core
    add_action( 'init', array( $this, 'core_init' ) );
    add_action( 'init', array( $this, 'cs_assets' ) );

  }

	public function core_init() {

    require_once SWEETCORE_ADMIN_DIR . '/init.php';

    // posttypes
    $clients = new PostTypes\CS_Clients();
    $reviews = new PostTypes\CS_Slider;
		$benefits = new PostTypes\CS_Projects();
		$gallery = new PostTypes\CS_Gallery();

		$brands = new PostTypes\CS_Tax_Brand();

		// shortcodes
		$shortcodes = new Shortcodes\CS_Shortcodes();
    $vc_custom = new VC_Custom\CS_VC_Extend();

		// helpers
		$functions         = new Helpers\CS_Functions();
		$seo               = new Helpers\CS_Seo();

		// Image Sizes
		$this->cs_image_sizes();
		$this->cs_register_sidebars();
		$this->cs_register_menus();


	}

	function cs_assets() {

		if ( ! in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ) {
			if ( ! is_admin() ) {
				add_action( 'wp_enqueue_scripts', array( $this, 'cs_enqueue_styles_head' ), 0 );
				add_action( 'wp_footer', array( $this, 'cs_enqueue_styles_footer' ), 0 );
				add_action( 'wp_enqueue_scripts', array( $this, 'cs_enqueue_scripts' ), 0 );
			}
		}

	}

	function cs_enqueue_styles_footer() {

	}

	function cs_enqueue_styles_head() {

	}

	function cs_enqueue_scripts() {

	}

  function cs_image_sizes(){

    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( '226', '177', true );
    add_image_size('shop_catalog', '270', '190', true);
    add_image_size('full-thumb', '768', '9999');
    add_image_size('small-thumb', '80', '9999', false);

  }

  private function cs_register_sidebars(){

    add_filter('widget_text', 'do_shortcode');

    register_sidebar( array(

        'id'          => 'blog-sidebar',
        'name'        => __( 'Blog Sidebar', 'cs' ),
        'description' => __( 'Blog Sidebar', 'cs' ),
        'before_widget' => '<div id="%1$s" class="widget widget-right %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => "</h3>\n",

    ) );

    $widgets = new Widgets\CS_Widgets();

  }

  private function cs_register_menus(){

    // Menu Support
    add_theme_support('Menus');

    register_nav_menus(
        array(
            'main' => 'Main menu',
            'categories' => 'Categories menu',
            'footer_menu' => 'Footer menu',
        )
    );

  }


}

$sweetcore = new CS_Core();